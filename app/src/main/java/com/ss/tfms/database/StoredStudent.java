    package com.ss.tfms.database;

    import java.io.Serializable;

    public class StoredStudent implements Serializable{

        private int _std_id = 0;
        private int _std_batch_id = 0;
        private String _std_name = "";
        private String _std_num = "";
        private String _std_parents_name  = "";
        private String _std_parents_num  = "";
        private String _std_address  = "";
        private String _std_school  = "";
        private String _std_class  = "";
        private String _std_group  = "";
        private String _std_blood_grp  = "";
        private String _std_add_time  = "";
        private String _std_fee  = "";
        private int _std_fee_status  = 0;
        private int _srv_status  = 0;
        
    //constractor
    public StoredStudent(){

    }
    public StoredStudent( int std_id,
                          int std_batch_id,
                          String std_name,
                          String std_num,
                          String std_parents_name ,
                          String std_parents_num ,
                          String std_address ,
                          String std_school ,
                          String std_class ,
                          String std_group ,
                          String std_blood_grp ,
                          String std_add_time,
                          String std_fee,
                          int status){

         this._std_id = std_id ;
         this._std_batch_id = std_batch_id;
         this._std_name = std_name;
         this._std_num = std_num;
         this._std_parents_name  = std_parents_name;
         this._std_parents_num  = std_parents_num;
         this._std_address  = std_address;
         this._std_school  = std_school;
         this._std_class  = std_class;
         this._std_group  = std_group;
         this._std_blood_grp  = std_blood_grp;
         this._std_add_time  = std_add_time;
         this._std_fee  = std_fee;
         this._srv_status = status;

    }



        public void set_std_id(int id){
            this._std_id = id;
        }
        public int get_std_id(){
            return this._std_id;
        }

        public void set_std_batch_id(int id){
            this._std_batch_id = id;
        }
        public int get_std_batch_id(){
            return this._std_batch_id;
        }

        public void set_std_name(String name){
            this._std_name = name;
        }
        public String get_std_name(){
            return this._std_name;
        }

        public void set_std_num(String num){
            this._std_num = num;
        }
        public String get_std_num(){
            return this._std_num;
        }

        public void set_std_parents_name(String name){
            this._std_parents_name = name;
        }
        public String get_std_parents_name(){
            return this._std_parents_name;
        }


        public void set_std_parents_num(String num){
            this._std_parents_num = num;
        }
        public String get_std_parents_num(){
            return this._std_parents_num;
        }



        public void set_std_address(String addr){
            this._std_address = addr;
        }
        public String get_std_address(){
            return this._std_address;
        }


        public void set_std_school(String scl){
            this._std_school = scl;
        }
        public String get_std_school(){
            return this._std_school;
        }

        public void set_std_class(String cls){
            this._std_class = cls;
        }
        public String get_std_class(){
            return this._std_class;
        }


        public void set_std_group(String group){
            this._std_group = group;
        }
        public String get_std_group(){
            return this._std_group;
        }


        public void set_std_blood_grp(String group){
            this._std_blood_grp = group;
        }
        public String get_std_blood_grp(){
            return this._std_blood_grp;
        }


        public void set_std_add_time(String time){
            this._std_add_time = time;
        }
        public String get_std_add_time(){
            return this._std_add_time;
        }

        public void set_std_fee(String fee){
            this._std_fee = fee;
        }
        public String get_std_fee(){
            return this._std_fee;
        }

        public void set_std_fee_status(int fee){
            this._std_fee_status = fee;
        }
        public int get_std_fee_status(){
            return this._std_fee_status;
        }

        public void set_srv_status(int status){
            this._srv_status = status;
        }
        public int get_srv_status(){
            return this._srv_status;
        }





    }