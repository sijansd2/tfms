package com.ss.tfms.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHandler extends SQLiteOpenHelper {
 
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "videocall.DB";


    private SQLiteDatabase db;
    private final Context context;

    public DBHandler(Context aContext) {
        super(aContext, DATABASE_NAME, null, DATABASE_VERSION);
        context = aContext;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DBTables.STUDENT_Table);
        db.execSQL(DBTables.BATCH_Table);
        db.execSQL(DBTables.TUITION_FEE_Table);
        
        System.out.println("+++++++++++++++++++++ALL Tables are created");

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " +DBTables.STUDENT_Table);
        db.execSQL("DROP TABLE IF EXISTS " +DBTables.BATCH_Table);
        db.execSQL("DROP TABLE IF EXISTS " +DBTables.TUITION_FEE_Table);

        onCreate(db);
    }

}