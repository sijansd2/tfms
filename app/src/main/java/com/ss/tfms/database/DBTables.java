package com.ss.tfms.database;

/**
 * Created by sijan on 11/24/15.
 */
public class DBTables {



    /* BATCH Table */
    public static final String BATCH_TABLE_NAME = "Table_BATCH";

    public static final String BATCH_ID_KEY = "batch_id";
    public static final String BATCH_NAME_KEY = "batch_name";
    public static final String BATCH_DATE_KEY = "batch_date";
    public static final String BATCH_DAY_KEY = "batch_day";
    public static final String BATCH_TIME_KEY = "batch_time";
    public static final String BATCH_STUDENT_COUNT_KEY = "batch_students_count";
    public static final String BATCH_SRV_STATUS_KEY = "batch_srv_status";

    public static final String BATCH_Table ="CREATE TABLE IF NOT EXISTS "
            + BATCH_TABLE_NAME
            + " ("
            + BATCH_ID_KEY              + " INTEGER,"
            + BATCH_NAME_KEY		    + " TEXT,"
            + BATCH_DATE_KEY		    + " TEXT,"
            + BATCH_DAY_KEY             + " TEXT,"
            + BATCH_TIME_KEY	        + " TEXT,"
            + BATCH_STUDENT_COUNT_KEY	+ " TEXT,"
            + BATCH_SRV_STATUS_KEY	    + " INTEGER"
            + ");";
    

    /* STUDENT Table */
    public static final String STUDENT_TABLE_NAME = "Table_Student";

    public static final String STUDENT_ID_KEY = "std_id";
    public static final String STUDENT_BATCH_ID_KEY = "std_batch_id";
    public static final String STUDENT_NAME_KEY = "std_name";
    public static final String STUDENT_NUM_KEY = "std_num";
    public static final String STUDENT_FEE_KEY = "std_fee";
    public static final String STUDENT_PARENTS_NAME_KEY = "std_parents_name";
    public static final String STUDENT_PARENTS_NUM_KEY = "std_parents_num";
    public static final String STUDENT_ADDRESS_KEY = "std_address";
    public static final String STUDENT_SCHOOL_KEY = "std_school";
    public static final String STUDENT_CLASS_KEY = "std_class";
    public static final String STUDENT_GROUP_KEY = "std_group";
    public static final String STUDENT_BLOOD_GROUP_KEY = "std_blood_grp";
    public static final String STUDENT_ADD_TIME_KEY = "std_add_time";
    public static final String STUDENT_SRV_STATUS_KEY = "std_srv_status";

    public static final String STUDENT_Table ="CREATE TABLE IF NOT EXISTS "
            + STUDENT_TABLE_NAME
            + " ("
            + STUDENT_ID_KEY             + " INTEGER,"
            + STUDENT_BATCH_ID_KEY		 + " INTEGER,"
            + STUDENT_NAME_KEY		     + " TEXT,"
            + STUDENT_NUM_KEY		     + " TEXT,"
            + STUDENT_FEE_KEY		     + " TEXT,"
            + STUDENT_PARENTS_NAME_KEY   + " TEXT,"
            + STUDENT_PARENTS_NUM_KEY	 + " TEXT,"
            + STUDENT_ADDRESS_KEY		 + " TEXT,"
            + STUDENT_SCHOOL_KEY		 + " TEXT,"
            + STUDENT_CLASS_KEY		     + " TEXT,"
            + STUDENT_GROUP_KEY		     + " TEXT,"
            + STUDENT_BLOOD_GROUP_KEY    + " TEXT,"
            + STUDENT_ADD_TIME_KEY		 + " TEXT,"
            + STUDENT_SRV_STATUS_KEY	 + " INTEGER"
            + ");";


    /* Tuition fee Table */
    public static final String TUITION_FEE_TABLE_NAME = "Table_TUITION_FEE";

    public static final String TUITION_FEE_ID_KEY = "tuition_fee_id";
    public static final String TUITION_FEE_STUDENT_ID_KEY = "tuition_fee_student_id";
    public static final String TUITION_FEE_STATUS_KEY = "tuition_fee_status";
    public static final String TUITION_FEE_DATE_KEY = "tuition_fee_date";
    public static final String TUITION_SRV_STATUS_KEY = "tuition_srv_status";

    public static final String TUITION_FEE_Table ="CREATE TABLE IF NOT EXISTS "
            + TUITION_FEE_TABLE_NAME
            + " ("
            + TUITION_FEE_ID_KEY                + " INTEGER,"
            + TUITION_FEE_STUDENT_ID_KEY		+ " INTEGER,"
            + TUITION_FEE_STATUS_KEY		    + " INTEGER,"
            + TUITION_FEE_DATE_KEY		        + " TEXT,"
            + TUITION_SRV_STATUS_KEY		    + " INTEGER"
            + ");";


 
}
