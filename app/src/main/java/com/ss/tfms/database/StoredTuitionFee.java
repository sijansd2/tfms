    package com.ss.tfms.database;

    import java.io.Serializable;

    public class StoredTuitionFee implements Serializable{

        private int _tuitionFeeId = 0;
        private int  _tuitionFeeStdId = 0;
        private int _tuitionFeeStatus = 0;
        private String _tuitionFeeDate = "";
        private int _srv_status  = 0;

    //constractor
    public StoredTuitionFee(){

    }
    public StoredTuitionFee(int batchId, int stdID, int status, String date, int srv_status ){

        this._tuitionFeeId = batchId;
        this._tuitionFeeStdId = stdID;
        this._tuitionFeeStatus = status;
        this._tuitionFeeDate = date;
        this._srv_status = srv_status;


    }

        public void set_Id(int id){
            this._tuitionFeeId = id;
        }
        public int get_Id(){
            return this._tuitionFeeId;
        }


        public void set_stdId(int stdid){
            this._tuitionFeeStdId = stdid;
        }
        public int get_stdId(){
            return this._tuitionFeeStdId;
        }


        public void set_status(int status){
            this._tuitionFeeStatus = status;
        }
        public int get_status(){
            return this._tuitionFeeStatus;
        }


        public void set_date(String date){
            this._tuitionFeeDate = date;
        }
        public String get_date(){
            return this._tuitionFeeDate;
        }

        public void set_srv_status(int status){
            this._srv_status = status;
        }
        public int get_srv_status(){
            return this._srv_status;
        }

    }