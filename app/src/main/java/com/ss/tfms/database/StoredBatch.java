    package com.ss.tfms.database;

    import java.io.Serializable;

    public class StoredBatch implements Serializable{

        private int _batchId = 0;
        private String _batchName = "";
        private String _batchDay = "";
        private String _batchTime = "";
        private String _batchMonth = "";
        private String _studentCount  = "";
        private int _srv_status  = 0;



    //constractor
    public StoredBatch(){

    }
    public StoredBatch(int batchId, String batchName,String batchDay,String batchTime,String batchMonth,String studentCount, int srv_status){

        this._batchId = batchId;
        this._batchName = batchName;
        this._batchDay = batchDay;
        this._batchTime = batchTime;
        this._batchMonth = batchMonth;
        this._studentCount = studentCount;
        this._srv_status = srv_status;


    }

        public void set_batchId(int id){
            this._batchId = id;
        }
        public int get_batchId(){
            return this._batchId;
        }


        public void set_batchName(String batchName){
            this._batchName = batchName;
        }
        public String get_batchName(){
            return this._batchName;
        }


        public void set_batchDay(String batchDay){
            this._batchDay = batchDay;
        }
        public String get_batchDay(){
            return this._batchDay;
        }


        public void set_batchTime(String batchTime){
            this._batchTime = batchTime;
        }
        public String get_batchTime(){
            return this._batchTime;
        }


        public void set_batchMonth(String batchMonth){
            this._batchMonth = batchMonth;
        }
        public String get_batchMonth(){
            return this._batchMonth;
        }


        public void set_studentCount(String studentCount){
            this._studentCount = studentCount;
        }
        public String get_studentCount(){
            return this._studentCount;
        }

        public void set_srv_status(int status){
            this._srv_status = status;
        }
        public int get_srv_status(){
            return this._srv_status;
        }



    }