    package com.ss.tfms.database;

    public class StoredHomeList {

      private int _id;
      private String _name;
      private String _number;
      private String _imageUri;
        private String _datetime;
      private int _flagForCategory;


    //constractor
    public StoredHomeList(){

    }
    public StoredHomeList(int id, String name, String number, String imageUri, String datetime, int flagForCategory){
        this._id = id;
        this._name = name;
        this._number = number;
        this._imageUri = imageUri;
        this._datetime = datetime;
        this._flagForCategory = flagForCategory;


    }

    //setting id
    public void setId(int id){
      this._id = id;
    }
    //getting id
    public int getId(){
      return this._id;
    }



      //setting name
      public void setName(String name){
        this._name = name;
      }
      //getting name
      public String getName(){
        return this._name;
      }



      //setting number
      public void setNumber(String number){
        this._number = number;
      }
      //getting number
      public String getNumber(){
        return this._number;
      }



      //setting nickname
      public void setImageUri(String imageUri){
        this._imageUri = imageUri;
      }
      //getting nickname
      public String getImageUri(){
        return this._imageUri;
      }


        //setting nickname
        public void setDatetime(String datetime){
            this._datetime = datetime;
        }
        //getting nickname
        public String getDatetime(){
            return this._datetime;
        }


      //setting flagForCategory
      public void setFlagForCategory(int flagForCategory){
        this._flagForCategory = flagForCategory;
      }
      //getting flagForCategory
      public int getFlagForCategory(){
        return this._flagForCategory;
      }



    }