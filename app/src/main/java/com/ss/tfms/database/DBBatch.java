package com.ss.tfms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ss.tfms.utils.ZemSettings;

import java.util.ArrayList;
import java.util.List;

    public class DBBatch extends DBHandler {

        Context mContext;

    public DBBatch(Context context){
        super(context);
        mContext = context;
    }

    // Adding new User
    public boolean addBatch(StoredBatch item) {

    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
        ZemSettings zem = new ZemSettings(mContext);

        if(item.get_batchId() != 0){
            values.put(DBTables.BATCH_ID_KEY, item.get_batchId());
            zem.setLastBatchCount(item.get_batchId());
        }else{
            values.put(DBTables.BATCH_ID_KEY, zem.getLastBatchCount()+1);
            zem.setLastBatchCount(zem.getLastBatchCount()+1);
        }
        zem.save();

        values.put(DBTables.BATCH_NAME_KEY, item.get_batchName());
        values.put(DBTables.BATCH_DATE_KEY, item.get_batchMonth());
        values.put(DBTables.BATCH_DAY_KEY, item.get_batchDay());
        values.put(DBTables.BATCH_TIME_KEY, item.get_batchTime());
        values.put(DBTables.BATCH_STUDENT_COUNT_KEY, item.get_studentCount());
        values.put(DBTables.BATCH_SRV_STATUS_KEY, item.get_srv_status());

    db.insert(DBTables.BATCH_TABLE_NAME, null, values);
    db.close();

    return true;
    }



    //Getting All Batches
    public List<StoredBatch> getAllBatches() {
        List<StoredBatch> itemList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DBTables.BATCH_TABLE_NAME + " WHERE NOT "+DBTables.BATCH_SRV_STATUS_KEY+"='-1' ORDER BY " +DBTables.BATCH_ID_KEY+ " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                StoredBatch item = new StoredBatch();
                item.set_batchId(cursor.getInt(0));
                item.set_batchName(cursor.getString(1));
                item.set_batchMonth(cursor.getString(2));
                item.set_batchDay(cursor.getString(3));
                item.set_batchTime(cursor.getString(4));
                item.set_studentCount(cursor.getString(5));
                item.set_srv_status(cursor.getInt(6));

                //if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
                itemList.add(item);

                } while (cursor.moveToNext());
    }		cursor.close();
            db.close();
            return itemList;
    }



        //Getting All Batches
        public List<StoredBatch> getUploadAbleBatches() {
            List<StoredBatch> itemList = new ArrayList<>();
            String selectQuery = "SELECT * FROM " + DBTables.BATCH_TABLE_NAME + " WHERE "+DBTables.BATCH_SRV_STATUS_KEY+"='1'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredBatch item = new StoredBatch();
                    item.set_batchId(cursor.getInt(0));
                    item.set_batchName(cursor.getString(1));
                    item.set_batchMonth(cursor.getString(2));
                    item.set_batchDay(cursor.getString(3));
                    item.set_batchTime(cursor.getString(4));
                    item.set_studentCount(cursor.getString(5));
                    item.set_srv_status(cursor.getInt(6));

                    //if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
                    itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


        public List<StoredBatch> getAllDeletedBatches() {
            List<StoredBatch> itemList = new ArrayList<>();
            String selectQuery = "SELECT * FROM " + DBTables.BATCH_TABLE_NAME +" WHERE "+DBTables.BATCH_SRV_STATUS_KEY+" = '-1'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredBatch item = new StoredBatch();
                    item.set_batchId(cursor.getInt(0));
                    item.set_batchName(cursor.getString(1));
                    item.set_batchMonth(cursor.getString(2));
                    item.set_batchDay(cursor.getString(3));
                    item.set_batchTime(cursor.getString(4));
                    item.set_studentCount(cursor.getString(5));
                    item.set_srv_status(cursor.getInt(6));

                    itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


        //Getting single Batch
        public StoredBatch getSingleBatch(int batchId) {
            StoredBatch item;
            String selectQuery = "SELECT * FROM " + DBTables.BATCH_TABLE_NAME + " WHERE "+DBTables.BATCH_ID_KEY+" = '"+batchId+"'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();

                    item = new StoredBatch();
                    item.set_batchId(cursor.getInt(0));
                    item.set_batchName(cursor.getString(1));
                    item.set_batchMonth(cursor.getString(2));
                    item.set_batchDay(cursor.getString(3));
                    item.set_batchTime(cursor.getString(4));
                    item.set_studentCount(cursor.getString(5));
                    item.set_srv_status(cursor.getInt(6));


            	cursor.close();
            db.close();
            return item;
        }


        //Getting All BatchNames
        public ArrayList<StoredBatch> getAllBatchNames() {
            ArrayList<StoredBatch> itemList = new ArrayList<>();
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.query(true,DBTables.BATCH_TABLE_NAME,new String[]{DBTables.BATCH_ID_KEY, DBTables.BATCH_NAME_KEY},null,null,null,null,DBTables.BATCH_ID_KEY+" DESC",null);

            if (cursor.moveToFirst()) {
                do {
                    StoredBatch item = new StoredBatch();
                    item.set_batchId(cursor.getInt(0));
                    item.set_batchName(cursor.getString(1));

                    itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


    // Delete User
    public void deleteBatch(StoredBatch item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DBTables.BATCH_TABLE_NAME, DBTables.BATCH_ID_KEY + " = ?", new String[]{String.valueOf(item.get_batchId())});
        db.close();
    }


    //Check if batch name is exists
    public boolean CheckIsExist(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + DBTables.BATCH_TABLE_NAME + " where " + DBTables.BATCH_NAME_KEY + " = " + "'"+name+"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


        //update single student
        public void updateBatch(StoredBatch item){

            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DBTables.BATCH_NAME_KEY, item.get_batchName());
            values.put(DBTables.BATCH_DATE_KEY, item.get_batchMonth());
            values.put(DBTables.BATCH_DAY_KEY, item.get_batchDay());
            values.put(DBTables.BATCH_TIME_KEY, item.get_batchTime());
            values.put(DBTables.BATCH_STUDENT_COUNT_KEY, item.get_studentCount());
            values.put(DBTables.BATCH_SRV_STATUS_KEY, item.get_srv_status());

            db.update(DBTables.BATCH_TABLE_NAME, values, DBTables.BATCH_ID_KEY + "= '" + item.get_batchId() +"'", null);
            db.close();

        }






        //Check if batch name is exists
        public int getBatchCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.BATCH_TABLE_NAME+" WHERE NOT "+DBTables.BATCH_SRV_STATUS_KEY+" = '-1'" ;
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }
        //Check if batch name is exists
        public int getInsertAbleBatchCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.BATCH_TABLE_NAME;
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }

        //Check if batch name is exists
        public int getUpdateableBatchCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.BATCH_TABLE_NAME +" WHERE "+DBTables.BATCH_SRV_STATUS_KEY+" = '1'";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }


        public int getDeleteableBatchCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.BATCH_TABLE_NAME +" WHERE "+DBTables.BATCH_SRV_STATUS_KEY+" = '-1'";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }


    }


