package com.ss.tfms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ss.tfms.utils.ZemSettings;

import java.util.ArrayList;
import java.util.List;

    public class DBTuitionFee extends DBHandler {

        Context mContext;

    public DBTuitionFee(Context context){
        super(context);
        mContext = context;
    }

    // Adding new User
    public boolean addTuitionFee(StoredTuitionFee item) {

        if(!CheckIfExist(item)) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();


            ZemSettings zem = new ZemSettings(mContext);

            if(item.get_Id() != 0){
                values.put(DBTables.TUITION_FEE_ID_KEY, item.get_Id());
                zem.setLastFeeCount(item.get_Id());
            }else{
                values.put(DBTables.TUITION_FEE_ID_KEY, zem.getLastFeeCount()+1);
                zem.setLastFeeCount(zem.getLastFeeCount()+1);
            }
            zem.save();

            values.put(DBTables.TUITION_FEE_STUDENT_ID_KEY, item.get_stdId());
            values.put(DBTables.TUITION_FEE_STATUS_KEY, item.get_status());
            values.put(DBTables.TUITION_FEE_DATE_KEY, item.get_date());
            values.put(DBTables.TUITION_SRV_STATUS_KEY, item.get_srv_status());

            db.insert(DBTables.TUITION_FEE_TABLE_NAME, null, values);
            db.close();
        }else{
            updateTuitionFee(item);
        }

    return true;
    }



    //Getting All Batches
    public List<StoredTuitionFee> getAllTuitionFee() {
        List<StoredTuitionFee> itemList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DBTables.TUITION_FEE_TABLE_NAME ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                StoredTuitionFee item = new StoredTuitionFee();
                item.set_Id(cursor.getInt(0));
                item.set_stdId(cursor.getInt(1));
                item.set_status(cursor.getInt(2));
                item.set_date(cursor.getString(3));
                item.set_srv_status(cursor.getInt(4));

                if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
                itemList.add(item);

                } while (cursor.moveToNext());
    }		cursor.close();
            db.close();
            return itemList;
    }

        //Getting All Batches
        public List<StoredTuitionFee> getUploadAbleTuitionFee() {
            List<StoredTuitionFee> itemList = new ArrayList<>();
            String selectQuery = "SELECT * FROM " + DBTables.TUITION_FEE_TABLE_NAME + " WHERE "+DBTables.TUITION_SRV_STATUS_KEY+"='1'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredTuitionFee item = new StoredTuitionFee();
                    item.set_Id(cursor.getInt(0));
                    item.set_stdId(cursor.getInt(1));
                    item.set_status(cursor.getInt(2));
                    item.set_date(cursor.getString(3));
                    item.set_srv_status(cursor.getInt(4));

                    if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
                        itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


        //Getting All Batches
        public List<StoredTuitionFee> getAllDeleteableTuitionFee() {
            List<StoredTuitionFee> itemList = new ArrayList<>();
            String selectQuery = "SELECT * FROM " + DBTables.TUITION_FEE_TABLE_NAME +" WHERE "+DBTables.TUITION_SRV_STATUS_KEY+" = '-1'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredTuitionFee item = new StoredTuitionFee();
                    item.set_Id(cursor.getInt(0));
                    item.set_stdId(cursor.getInt(1));
                    item.set_status(cursor.getInt(2));
                    item.set_date(cursor.getString(3));
                    item.set_srv_status(cursor.getInt(4));

                    if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
                        itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }

        // Delete User
        public void deleteTuitionFee(StoredTuitionFee item) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(DBTables.TUITION_FEE_TABLE_NAME, DBTables.TUITION_FEE_DATE_KEY + " = ? AND "+DBTables.TUITION_FEE_STUDENT_ID_KEY+" = ? ",
                    new String[]{String.valueOf(item.get_date()),String.valueOf(item.get_stdId())});
            db.close();
        }


    //Check if batch name is exists
    public boolean CheckIfExist(StoredTuitionFee item) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + DBTables.TUITION_FEE_TABLE_NAME + " where " + DBTables.TUITION_FEE_STUDENT_ID_KEY + " = " + "'"+item.get_stdId()
                +"' AND "+ DBTables.TUITION_FEE_DATE_KEY + " = " + "'"+item.get_date()+"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }



    //update single student
    public void updateTuitionFee(StoredTuitionFee item){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(DBTables.STUDENT_ID_KEY, item.get_std_id());
         values.put(DBTables.TUITION_FEE_STUDENT_ID_KEY, item.get_stdId());
        values.put(DBTables.TUITION_FEE_STATUS_KEY, item.get_status());
        values.put(DBTables.TUITION_FEE_DATE_KEY, item.get_date());
        values.put(DBTables.TUITION_SRV_STATUS_KEY, item.get_srv_status());

        db.update(DBTables.TUITION_FEE_TABLE_NAME, values, DBTables.TUITION_FEE_DATE_KEY + " = ? AND "+DBTables.TUITION_FEE_STUDENT_ID_KEY+" = ? ",
                new String[]{String.valueOf(item.get_date()),String.valueOf(item.get_stdId())});
        db.close();

    }


        public List<StoredStudent> getStdWithPayment(){
            List<StoredStudent> itemList = new ArrayList<>();
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select "+DBTables.STUDENT_ID_KEY+","+DBTables.STUDENT_NAME_KEY+","+DBTables.STUDENT_NUM_KEY+","+DBTables.STUDENT_FEE_KEY+","+DBTables.TUITION_FEE_STATUS_KEY+
                    " from " + DBTables.STUDENT_TABLE_NAME + " INNER JOIN  " + DBTables.TUITION_FEE_TABLE_NAME + " ON "+
                    DBTables.STUDENT_ID_KEY+"="+DBTables.TUITION_FEE_STUDENT_ID_KEY;
            Cursor cursor = db.rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredStudent item = new StoredStudent();
                    item.set_std_id(cursor.getInt(0));
                    item.set_std_name(cursor.getString(1));
                    item.set_std_num(cursor.getString(2));
                    item.set_std_fee(cursor.getString(3));
                    item.set_std_fee_status(cursor.getInt(4));
                    itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


        public List<StoredStudent> getStudentsOfBatchId(int batchId, String date){
            List<StoredStudent> itemList = new ArrayList<>();
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select "
                    +DBTables.STUDENT_ID_KEY+","
                    +DBTables.STUDENT_BATCH_ID_KEY+","
                    +DBTables.STUDENT_NAME_KEY+","
                    +DBTables.STUDENT_NUM_KEY+","
                    +DBTables.STUDENT_PARENTS_NUM_KEY+","
                    +DBTables.STUDENT_FEE_KEY+","
                    +DBTables.STUDENT_SRV_STATUS_KEY+
                    " from "
                    +DBTables.STUDENT_TABLE_NAME +" WHERE "+DBTables.STUDENT_BATCH_ID_KEY+" = '"+batchId+"' AND NOT "+DBTables.STUDENT_SRV_STATUS_KEY+"='-1'  ORDER BY " +DBTables.STUDENT_ID_KEY+ " DESC";

            Cursor cursor = db.rawQuery(Query, null);
            if (cursor.moveToFirst()) {
                do {
                    StoredStudent item = new StoredStudent();
                    item.set_std_id(cursor.getInt(0));
                    item.set_std_batch_id(cursor.getInt(1));
                    item.set_std_name(cursor.getString(2));
                    item.set_std_num(cursor.getString(3));
                    item.set_std_parents_num(cursor.getString(4));
                    item.set_std_fee(cursor.getString(5));
                    item.set_std_fee_status(getTuitionFeeStatus(cursor.getInt(0), date, db));
                    item.set_srv_status(cursor.getInt(6));
                    itemList.add(item);

                } while (cursor.moveToNext());
            }		cursor.close();
            db.close();
            return itemList;
        }


        int getTuitionFeeStatus(int id, String date, SQLiteDatabase db){

            int aa=0;
            String query = "SELECT "+DBTables.TUITION_FEE_STATUS_KEY+","+DBTables.TUITION_FEE_DATE_KEY+
                    " FROM "+DBTables.TUITION_FEE_TABLE_NAME+" WHERE "
                    +DBTables.TUITION_FEE_STUDENT_ID_KEY+"='"+id+"'";
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {

                    String ss = cursor.getString(1);
                    if(ss.equals(date)) {
                        aa = cursor.getInt(0);
                        break;
                    }
                } while (cursor.moveToNext());
            }

            cursor.close();
            return aa;
        }


        //Check if batch name is exists
        public int getFeeCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.TUITION_FEE_TABLE_NAME ;
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }


        //Check if batch name is exists
        public int getUpdateableFeeCount() {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query = "Select * from " + DBTables.TUITION_FEE_TABLE_NAME +" WHERE "+DBTables.TUITION_SRV_STATUS_KEY+" = '1'";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() > 0){
                int i = cursor.getCount();
                cursor.close();
                return i;
            }
            cursor.close();
            return 0;
        }

    }


