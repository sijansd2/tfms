package com.ss.tfms.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ss.tfms.utils.ZemSettings;

import java.util.ArrayList;
import java.util.List;

	public class DBStudent extends DBHandler {

		Context mContext;

	public DBStudent(Context context){
		super(context);
		mContext = context;
	}

	// Adding new User
	public boolean addStudent(StoredStudent item) {

	SQLiteDatabase db = this.getWritableDatabase();
	ContentValues values = new ContentValues();

		ZemSettings zem = new ZemSettings(mContext);

		if(item.get_std_id() != 0){
			values.put(DBTables.STUDENT_ID_KEY, item.get_std_id());
			zem.setLastStdCount(item.get_std_id());
		}else{
			values.put(DBTables.STUDENT_ID_KEY, zem.getLastStdCount()+1);
			zem.setLastStdCount(zem.getLastStdCount()+1);
		}
		zem.save();

		values.put(DBTables.STUDENT_BATCH_ID_KEY, item.get_std_batch_id());
		values.put(DBTables.STUDENT_NAME_KEY, item.get_std_name());
		values.put(DBTables.STUDENT_NUM_KEY, item.get_std_num());
		values.put(DBTables.STUDENT_PARENTS_NAME_KEY, item.get_std_parents_name());
		values.put(DBTables.STUDENT_PARENTS_NUM_KEY, item.get_std_parents_num());
		values.put(DBTables.STUDENT_ADDRESS_KEY, item.get_std_address());
		values.put(DBTables.STUDENT_SCHOOL_KEY, item.get_std_school());
		values.put(DBTables.STUDENT_CLASS_KEY, item.get_std_class());
		values.put(DBTables.STUDENT_GROUP_KEY, item.get_std_group());
		values.put(DBTables.STUDENT_BLOOD_GROUP_KEY, item.get_std_blood_grp());
		values.put(DBTables.STUDENT_ADD_TIME_KEY, item.get_std_add_time());
		values.put(DBTables.STUDENT_FEE_KEY, item.get_std_fee());
		values.put(DBTables.STUDENT_SRV_STATUS_KEY, item.get_srv_status());

	db.insert(DBTables.STUDENT_TABLE_NAME, null, values);
	db.close();

	return true;
	}



	//Getting All User
	public List<StoredStudent> getAllStudents() {
		List<StoredStudent> itemList = new ArrayList<>();
		//String selectQuery = "SELECT  * FROM " + DBTables.User_TABLE_NAME;
		String selectQuery = "SELECT * FROM " + DBTables.STUDENT_TABLE_NAME;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				StoredStudent item = new StoredStudent();
				item.set_std_id(cursor.getInt(0));
				item.set_std_batch_id(cursor.getInt(1));
				item.set_std_name(cursor.getString(2));
				item.set_std_num(cursor.getString(3));
				item.set_std_fee(cursor.getString(4));
				item.set_std_parents_name(cursor.getString(5));
				item.set_std_parents_num(cursor.getString(6));
				item.set_std_address(cursor.getString(7));
				item.set_std_school(cursor.getString(8));
				item.set_std_class(cursor.getString(9));
				item.set_std_group(cursor.getString(10));
				item.set_std_blood_grp(cursor.getString(11));
				item.set_std_add_time(cursor.getString(12));
				item.set_srv_status(cursor.getInt(13));

				if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
				itemList.add(item);

				} while (cursor.moveToNext());
	}		cursor.close();
			db.close();
			return itemList;
	}


		//Getting All User
		public List<StoredStudent> getUploadAbleStudents() {
			List<StoredStudent> itemList = new ArrayList<>();
			//String selectQuery = "SELECT  * FROM " + DBTables.User_TABLE_NAME;
			String selectQuery = "SELECT * FROM " + DBTables.STUDENT_TABLE_NAME + " WHERE "+DBTables.STUDENT_SRV_STATUS_KEY+"='1'";
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					StoredStudent item = new StoredStudent();
					item.set_std_id(cursor.getInt(0));
					item.set_std_batch_id(cursor.getInt(1));
					item.set_std_name(cursor.getString(2));
					item.set_std_num(cursor.getString(3));
					item.set_std_fee(cursor.getString(4));
					item.set_std_parents_name(cursor.getString(5));
					item.set_std_parents_num(cursor.getString(6));
					item.set_std_address(cursor.getString(7));
					item.set_std_school(cursor.getString(8));
					item.set_std_class(cursor.getString(9));
					item.set_std_group(cursor.getString(10));
					item.set_std_blood_grp(cursor.getString(11));
					item.set_std_add_time(cursor.getString(12));
					item.set_srv_status(cursor.getInt(13));

					if(item.get_srv_status() != ServerUpLoad.MSG_TYPE.DELETE_STATUS)
						itemList.add(item);

				} while (cursor.moveToNext());
			}		cursor.close();
			db.close();
			return itemList;
		}


		//Getting All User
		public List<StoredStudent> getAllDeleteableStudents() {
			List<StoredStudent> itemList = new ArrayList<>();
			//String selectQuery = "SELECT  * FROM " + DBTables.User_TABLE_NAME;
			String selectQuery = "SELECT * FROM " + DBTables.STUDENT_TABLE_NAME+" WHERE "+DBTables.STUDENT_SRV_STATUS_KEY+" = '-1'";
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					StoredStudent item = new StoredStudent();
					item.set_std_id(cursor.getInt(0));
					item.set_std_batch_id(cursor.getInt(1));
					item.set_std_name(cursor.getString(2));
					item.set_std_num(cursor.getString(3));
					item.set_std_fee(cursor.getString(4));
					item.set_std_parents_name(cursor.getString(5));
					item.set_std_parents_num(cursor.getString(6));
					item.set_std_address(cursor.getString(7));
					item.set_std_school(cursor.getString(8));
					item.set_std_class(cursor.getString(9));
					item.set_std_group(cursor.getString(10));
					item.set_std_blood_grp(cursor.getString(11));
					item.set_std_add_time(cursor.getString(12));
					item.set_srv_status(cursor.getInt(13));

					itemList.add(item);

				} while (cursor.moveToNext());
			}		cursor.close();
			db.close();
			return itemList;
		}


	// Delete User
	public void deleteStudent(StoredStudent item) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DBTables.STUDENT_TABLE_NAME, DBTables.STUDENT_ID_KEY + " = ?", new String[]{String.valueOf(item.get_std_id())});
		db.close();
	}


		// Delete User
		public void deleteAllStudentOfBatch(int batchId) {
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(DBTables.STUDENT_TABLE_NAME, DBTables.STUDENT_BATCH_ID_KEY + " = ?", new String[]{batchId+""});
			db.close();
		}


	//Check if student name is exists
	public boolean CheckIsExist(String name) {
		SQLiteDatabase db = this.getWritableDatabase();
		String Query = "Select * from " + DBTables.STUDENT_TABLE_NAME + " where " + DBTables.STUDENT_NAME_KEY + " = " + "'"+name+"'";
		Cursor cursor = db.rawQuery(Query, null);
		if(cursor.getCount() <= 0){
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}


		//get number of students in specific batch.
		public int getStudentCount(int batchId) {
			int studentCount = 0;
			SQLiteDatabase db = this.getWritableDatabase();
			String Query = "Select * from " + DBTables.STUDENT_TABLE_NAME + " where " + DBTables.STUDENT_BATCH_ID_KEY + " = " + "'"+batchId+"' AND NOT "+
					DBTables.STUDENT_SRV_STATUS_KEY+" = -1";
			Cursor cursor = db.rawQuery(Query, null);
			studentCount = cursor.getCount();
			cursor.close();
			db.close();
			return studentCount;
		}

		//get number of students in specific batch.
		public int getAllStudentCount() {
			int studentCount = 0;
			SQLiteDatabase db = this.getWritableDatabase();
			String Query = "Select * from " + DBTables.STUDENT_TABLE_NAME+ " WHERE NOT "+DBTables.STUDENT_SRV_STATUS_KEY+" = '-1'";
			Cursor cursor = db.rawQuery(Query, null);
			if(cursor.getCount() > 0){
				int i = cursor.getCount();
				cursor.close();
				return i;
			}
			cursor.close();
			return 0;
		}


		// Getting single Student info using id
		public StoredStudent getSingleStudent(int id) {

			String selectQuery = "SELECT * FROM " + DBTables.STUDENT_TABLE_NAME + " where " + DBTables.STUDENT_ID_KEY + " = " + "'"+id+"'";
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			StoredStudent item = new StoredStudent();

			if (cursor.moveToFirst()) {
				do {
					item.set_std_id(cursor.getInt(0));
					item.set_std_batch_id(cursor.getInt(1));
					item.set_std_name(cursor.getString(2));
					item.set_std_num(cursor.getString(3));
					item.set_std_parents_name(cursor.getString(4));
					item.set_std_parents_num(cursor.getString(5));
					item.set_std_address(cursor.getString(6));
					item.set_std_school(cursor.getString(7));
					item.set_std_class(cursor.getString(8));
					item.set_std_group(cursor.getString(9));
					item.set_std_blood_grp(cursor.getString(10));
					item.set_std_add_time(cursor.getString(11));
				} while (cursor.moveToNext());
			}
				cursor.close();
				db.close();
			return item;
		}



		//update single student
		public void updateStudent(StoredStudent item){

			SQLiteDatabase db = this.getWritableDatabase();

					ContentValues values = new ContentValues();
					values.put(DBTables.STUDENT_ID_KEY, item.get_std_id());
					values.put(DBTables.STUDENT_BATCH_ID_KEY, item.get_std_batch_id());
					values.put(DBTables.STUDENT_NAME_KEY, item.get_std_name());
					values.put(DBTables.STUDENT_NUM_KEY, item.get_std_num());
					values.put(DBTables.STUDENT_PARENTS_NAME_KEY, item.get_std_parents_name());
					values.put(DBTables.STUDENT_PARENTS_NUM_KEY, item.get_std_parents_num());
					values.put(DBTables.STUDENT_ADDRESS_KEY, item.get_std_address());
					values.put(DBTables.STUDENT_SCHOOL_KEY, item.get_std_school());
					values.put(DBTables.STUDENT_CLASS_KEY, item.get_std_class());
					values.put(DBTables.STUDENT_GROUP_KEY, item.get_std_group());
					values.put(DBTables.STUDENT_BLOOD_GROUP_KEY, item.get_std_blood_grp());
					values.put(DBTables.STUDENT_ADD_TIME_KEY, item.get_std_add_time());
					values.put(DBTables.STUDENT_FEE_KEY, item.get_std_fee());
					values.put(DBTables.STUDENT_SRV_STATUS_KEY, item.get_srv_status());

					db.update(DBTables.STUDENT_TABLE_NAME, values, DBTables.STUDENT_ID_KEY + "=" + item.get_std_id(), null);
			db.close();

		}


		public int getLastRowId(){

			String query = "SELECT "+DBTables.STUDENT_ID_KEY+" from "+DBTables.STUDENT_TABLE_NAME+" order by "+DBTables.STUDENT_ID_KEY+" DESC limit 1";
			int lastId=0;
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor c = db.rawQuery(query, null);
			if (c != null && c.moveToFirst()) {
				lastId = c.getInt(0); //The 0 is the column index, we only have 1 column, so the index is 0
			}
			c.close();
			db.close();

			return lastId;
		}

}


