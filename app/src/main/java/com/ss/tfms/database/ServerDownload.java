package com.ss.tfms.database;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;

import com.ss.tfms.activity.MainActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sijan on 4/7/2017.
 */

public class ServerDownload {

    private static final String TAG_SUCCESS = "success";
    public static ProgressDialog progress;

    public static class LINK_TYPE {
        public static String GET_BATCH = "http://navedimperio.com/tmsphpfiles/get_all_batch.php";
        public static String GET_STD = "http://navedimperio.com/tmsphpfiles/get_all_std.php";
        public static String GET_FEE = "http://navedimperio.com/tmsphpfiles/get_all_tfee.php";
    }

    //Load db info and new events
   public static class LoadBatchInfo extends AsyncTask<String, String, String> {

        Activity mContext;
        String db_version = "0";
        String new_events = "";

        public LoadBatchInfo(Activity context){
            mContext = context;
        }
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(mContext, "Processing", "Please wait...", true);
            progress.setCancelable(false);
        }

        protected String doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            JSONArray info = null;
            DBBatch db = new DBBatch(mContext);


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("api_key", ServerUpLoad.MSG_TYPE.API_KEY));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(LINK_TYPE.GET_BATCH, "POST", params);

            try {
                // Checking for SUCCESS TAG
                int success = 0;
                if(json != null) {
                    success = json.getInt(TAG_SUCCESS);
                }

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    info = json.getJSONArray("table_batch");

                    // looping through All Products
                    for (int i = 0; i < info.length(); i++) {
                        JSONObject c = info.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(DBTables.BATCH_ID_KEY);
                        String name = c.getString(DBTables.BATCH_NAME_KEY);
                        String date = c.getString(DBTables.BATCH_DATE_KEY);
                        String day = c.getString(DBTables.BATCH_DAY_KEY);
                        String time = c.getString(DBTables.BATCH_TIME_KEY);
                        String std_count = c.getString(DBTables.BATCH_STUDENT_COUNT_KEY);

                        StoredBatch item = new StoredBatch();

                        item.set_batchId(Integer.parseInt(id));
                        item.set_batchName(name);
                        item.set_batchMonth(date);
                        item.set_batchDay(day);
                        item.set_batchTime(time);
                        item.set_studentCount(std_count);
                        item.set_srv_status(ServerUpLoad.MSG_TYPE.NOT_ADD_STATUS);
                        db.addBatch(item);


                    }
                } else {
                    // no products found
                    // Launch Add New product Activity

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new_events;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            new LoadStdInfo(mContext).execute();
        }

    }


    //Load db info and new events
    public static class LoadStdInfo extends AsyncTask<String, String, String> {

        Context mContext;
        String db_version = "0";
        String new_events = "";

        public LoadStdInfo(Context context){
            mContext = context;
        }
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            JSONArray info = null;
            DBStudent db = new DBStudent(mContext);


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("api_key", ServerUpLoad.MSG_TYPE.API_KEY));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(LINK_TYPE.GET_STD, "POST", params);

            try {
                // Checking for SUCCESS TAG
                int success = 0;
                if(json != null) {
                    success = json.getInt(TAG_SUCCESS);
                }

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    info = json.getJSONArray("table_student");

                    // looping through All Products
                    for (int i = 0; i < info.length(); i++) {
                        JSONObject c = info.getJSONObject(i);

                        // Storing each json item in variable
                        String std_id = c.getString(DBTables.STUDENT_ID_KEY);
                        String batch_id = c.getString(DBTables.STUDENT_BATCH_ID_KEY);
                        String name = c.getString(DBTables.STUDENT_NAME_KEY);
                        String num = c.getString(DBTables.STUDENT_NUM_KEY);
                        String fee = c.getString(DBTables.STUDENT_FEE_KEY);
                        String pName = c.getString(DBTables.STUDENT_PARENTS_NAME_KEY);
                        String pnum = c.getString(DBTables.STUDENT_PARENTS_NUM_KEY);
                        String addr = c.getString(DBTables.STUDENT_ADDRESS_KEY);
                        String scl = c.getString(DBTables.STUDENT_SCHOOL_KEY);
                        String cls = c.getString(DBTables.STUDENT_CLASS_KEY);
                        String grp= c.getString(DBTables.STUDENT_GROUP_KEY);
                        String bGrp = c.getString(DBTables.STUDENT_BLOOD_GROUP_KEY);
                        String addTm = c.getString(DBTables.STUDENT_ADD_TIME_KEY);

                        StoredStudent item = new StoredStudent();
                        item.set_std_id(Integer.parseInt(std_id));
                        item.set_std_batch_id(Integer.parseInt(batch_id));
                        item.set_std_name(name);
                        item.set_std_num(num);
                        item.set_std_fee(fee);
                        item.set_std_parents_name(pName);
                        item.set_std_parents_num(pnum);
                        item.set_std_address(addr);
                        item.set_std_school(scl);
                        item.set_std_class(cls);
                        item.set_std_group(grp);
                        item.set_std_blood_grp(bGrp);
                        item.set_std_add_time(addTm);

                        db.addStudent(item);


                    }
                } else {
                    // no products found
                    // Launch Add New product Activity

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new_events;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            new LoadFeeInfo(mContext).execute();
        }

    }


    public static class LoadFeeInfo extends AsyncTask<String, String, String> {

        Context mContext;
        String new_events = "";

        public LoadFeeInfo(Context context){
            mContext = context;
        }
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            JSONArray info = null;
            DBTuitionFee db = new DBTuitionFee(mContext);


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("api_key", ServerUpLoad.MSG_TYPE.API_KEY));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(LINK_TYPE.GET_FEE, "POST", params);

            try {
                // Checking for SUCCESS TAG
                int success = 0;
                if(json != null) {
                    success = json.getInt(TAG_SUCCESS);
                }

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    info = json.getJSONArray("table_tuition_fee");

                    // looping through All Products
                    for (int i = 0; i < info.length(); i++) {
                        JSONObject c = info.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(DBTables.TUITION_FEE_ID_KEY);
                        String std_id = c.getString(DBTables.TUITION_FEE_STUDENT_ID_KEY);
                        String fee_status = c.getString(DBTables.TUITION_FEE_STATUS_KEY);
                        String date = c.getString(DBTables.TUITION_FEE_DATE_KEY);

                        StoredTuitionFee item = new StoredTuitionFee();

                        item.set_Id(Integer.parseInt(id));
                        item.set_stdId(Integer.parseInt(std_id));
                        item.set_status(Integer.parseInt(fee_status));
                        item.set_date(date);
                        item.set_srv_status(ServerUpLoad.MSG_TYPE.NOT_ADD_STATUS);
                        db.addTuitionFee(item);


                    }
                } else {
                    // no products found
                    // Launch Add New product Activity

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new_events;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
           // new LoadStdInfo(mContext).execute();
            Message msg = Message.obtain(MainActivity.handler, MainActivity.MSG_TYPE.DOWNLOAD_SUCCESS);
            msg.sendToTarget();
            progress.dismiss();
        }

    }

}
