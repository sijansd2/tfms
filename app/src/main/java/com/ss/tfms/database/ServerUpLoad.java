package com.ss.tfms.database;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;
import android.view.View;

import com.ss.tfms.activity.MainActivity;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sijan on 4/5/2017.
 */

public class ServerUpLoad {

    public static class RESPONSE_TYPE {

        public static String SUCCESS = "success";
        public static String UPDATE_SUCCESS = "update_success";
        public static String API_NOT_MATCH = "api_not_match";
    }

    public static class MSG_TYPE {
        public static int ADD_STATUS = 1;
        public static int NOT_ADD_STATUS = 0;
        public static int DELETE_STATUS = -1;

        public static String API_KEY = "navedimperio";
    }

    public static class LINK_TYPE{
        public static String BATCH_INSERT = "http://navedimperio.com/tmsphpfiles/batch_insert.php";
        public static String BATCH_DELETE = "http://navedimperio.com/tmsphpfiles/batch_delete.php";
        public static String STD_INSERT = "http://navedimperio.com/tmsphpfiles/std_insert.php";
        public static String STD_DELETE = "http://navedimperio.com/tmsphpfiles/std_delete.php";
        public static String FEE_INSERT = "http://navedimperio.com/tmsphpfiles/tuitionfee_insert.php";
        public static String FEE_DELETE = "http://navedimperio.com/tmsphpfiles/tuitionfee_delete.php";
    }

    String link = "";
    public static ProgressDialog progress;


    //**************** Batch upload part ***************//
    public static class BatchPostTask extends AsyncTask<String, String, String> {

        Context mContext;
        public BatchPostTask(Context context){
            this.mContext = context;
        }

        @Override
        protected String doInBackground(String... data) {

            DBBatch db = new DBBatch(mContext);
            List<StoredBatch> listItems = db.getUploadAbleBatches();
            // Create a new HttpClient and Post Header

            for(int i=0; i< listItems.size(); i++) {

                if (listItems.get(i).get_srv_status() == MSG_TYPE.ADD_STATUS) {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(LINK_TYPE.BATCH_INSERT);
                    String ss = "";
                    try {
                        //add data
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                        nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                        nameValuePairs.add(new BasicNameValuePair("batch_id", listItems.get(i).get_batchId() + ""));
                        nameValuePairs.add(new BasicNameValuePair("batch_name", listItems.get(i).get_batchName()));
                        nameValuePairs.add(new BasicNameValuePair("batch_date", listItems.get(i).get_batchMonth()));
                        nameValuePairs.add(new BasicNameValuePair("batch_day", listItems.get(i).get_batchDay()));
                        nameValuePairs.add(new BasicNameValuePair("batch_time", listItems.get(i).get_batchTime()));
                        nameValuePairs.add(new BasicNameValuePair("batch_students_count", listItems.get(i).get_studentCount()));

                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        //execute http post
                        HttpResponse response = httpclient.execute(httppost);
                        ss = EntityUtils.toString(response.getEntity());
                        if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                            listItems.get(i).set_srv_status(0);
                            db.updateBatch(listItems.get(i));
                        }
                        else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                            break;
                        }
                    } catch (ClientProtocolException e) {

                    } catch (IOException e) {

                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new StdDeletePostTask(mContext).execute();
        }
    }

    //**************** Batch delete part ***************//
    public static class BatchDeletePostTask extends AsyncTask<String, String, String> {

        Activity mContext;
        public BatchDeletePostTask(Activity context){
            this.mContext = context;
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(mContext, "Processing", "Please wait...", true);
            progress.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... data) {

            DBBatch db = new DBBatch(mContext);
            List<StoredBatch> listItems = db.getAllDeletedBatches();
            // Create a new HttpClient and Post Header

            for(int i=0; i< listItems.size(); i++) {

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(LINK_TYPE.BATCH_DELETE);
                    String ss = "";
                    try {
                        //add data
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                        nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                        nameValuePairs.add(new BasicNameValuePair("batch_id", listItems.get(i).get_batchId() + ""));

                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        //execute http post
                        HttpResponse response = httpclient.execute(httppost);
                        ss = EntityUtils.toString(response.getEntity());
                        if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                            db.deleteBatch(listItems.get(i));
                        }
                        else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                            break;
                        }
                    } catch (ClientProtocolException e) {

                    } catch (IOException e) {

                    }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new BatchPostTask(mContext).execute();
        }
    }

    //**************** Student upload part ***************//
    public static class StudentPostTask extends AsyncTask<String, String, String> {

        Context mContext;
        public StudentPostTask(Context context){
            this.mContext = context;
        }

        @Override
        protected String doInBackground(String... data) {

            DBStudent db = new DBStudent(mContext);
            List<StoredStudent> listItems = db.getUploadAbleStudents();

            for(int i=0; i< listItems.size(); i++) {
            // Create a new HttpClient and Post Header
            if (listItems.get(i).get_srv_status() == MSG_TYPE.ADD_STATUS) {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(LINK_TYPE.STD_INSERT);
                String ss = "";
                try {
                    //add data
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                    nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                    nameValuePairs.add(new BasicNameValuePair("std_id", listItems.get(i).get_std_id()+""));
                    nameValuePairs.add(new BasicNameValuePair("std_batch_id", listItems.get(i).get_std_batch_id()+""));
                    nameValuePairs.add(new BasicNameValuePair("std_name", listItems.get(i).get_std_name()));
                    nameValuePairs.add(new BasicNameValuePair("std_num", listItems.get(i).get_std_num()));
                    nameValuePairs.add(new BasicNameValuePair("std_fee", listItems.get(i).get_std_fee()));
                    nameValuePairs.add(new BasicNameValuePair("std_parents_name", listItems.get(i).get_std_parents_name()));
                    nameValuePairs.add(new BasicNameValuePair("std_parents_num", listItems.get(i).get_std_parents_num()));
                    nameValuePairs.add(new BasicNameValuePair("std_address", listItems.get(i).get_std_address()));
                    nameValuePairs.add(new BasicNameValuePair("std_school", listItems.get(i).get_std_school()));
                    nameValuePairs.add(new BasicNameValuePair("std_class", listItems.get(i).get_std_class()));
                    nameValuePairs.add(new BasicNameValuePair("std_group", listItems.get(i).get_std_group()));
                    nameValuePairs.add(new BasicNameValuePair("std_blood_grp", listItems.get(i).get_std_blood_grp()));
                    nameValuePairs.add(new BasicNameValuePair("std_add_time", listItems.get(i).get_std_add_time()));

                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    //execute http post
                    HttpResponse response = httpclient.execute(httppost);
                    ss = EntityUtils.toString(response.getEntity());
                    if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                        listItems.get(i).set_srv_status(0);
                        db.updateStudent(listItems.get(i));
                    }
                    else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                        break;
                    }
                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
            }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new TuitionFeePostTask(mContext).execute();
        }
    }


    //**************** Student delete part ***************//
    public static class StdDeletePostTask extends AsyncTask<String, String, String> {

        Context mContext;
        public StdDeletePostTask(Context context){
            this.mContext = context;
        }

        @Override
        protected String doInBackground(String... data) {

            DBStudent db = new DBStudent(mContext);
            List<StoredStudent> listItems = db.getAllDeleteableStudents();
            // Create a new HttpClient and Post Header

            for(int i=0; i< listItems.size(); i++) {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(LINK_TYPE.STD_DELETE);
                String ss = "";
                try {
                    //add data
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                    nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                    nameValuePairs.add(new BasicNameValuePair("std_id", listItems.get(i).get_std_id() + ""));

                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    //execute http post
                    HttpResponse response = httpclient.execute(httppost);
                    ss = EntityUtils.toString(response.getEntity());
                    if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                        db.deleteStudent(listItems.get(i));
                    }
                    else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                        break;
                    }
                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new StudentPostTask(mContext).execute();
        }
    }






    //**************** Tuition fee upload part ***************//
    public static class TuitionFeePostTask extends AsyncTask<String, String, String> {

        Context mContext;
        public TuitionFeePostTask(Context context){
            this.mContext = context;
        }

        @Override
        protected String doInBackground(String... data) {

            DBTuitionFee db = new DBTuitionFee(mContext);
            List<StoredTuitionFee> listItems = db.getUploadAbleTuitionFee();

            for(int i=0; i< listItems.size(); i++) {
                // Create a new HttpClient and Post Header
                if (listItems.get(i).get_srv_status() == MSG_TYPE.ADD_STATUS) {
                    // Create a new HttpClient and Post Header
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(LINK_TYPE.FEE_INSERT);
                    String ss = "";
                    try {
                        //add data
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                        nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                        nameValuePairs.add(new BasicNameValuePair("id", listItems.get(i).get_Id()+""));
                        nameValuePairs.add(new BasicNameValuePair("std_id", listItems.get(i).get_stdId()+""));
                        nameValuePairs.add(new BasicNameValuePair("status", listItems.get(i).get_status()+""));
                        nameValuePairs.add(new BasicNameValuePair("date", listItems.get(i).get_date()));

                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        //execute http post
                        HttpResponse response = httpclient.execute(httppost);
                        ss = EntityUtils.toString(response.getEntity());
                        if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                            listItems.get(i).set_srv_status(0);
                            db.updateTuitionFee(listItems.get(i));
                        }
                        else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                            break;
                        }
                    } catch (ClientProtocolException e) {

                    } catch (IOException e) {

                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            Message msg = Message.obtain(MainActivity.handler,MainActivity.MSG_TYPE.UPLOAD_SUCCESS);
            msg.sendToTarget();
        }
    }




    //**************** Student delete part ***************//
    public static class FeeDeletePostTask extends AsyncTask<String, String, String> {

        Context mContext;
        public FeeDeletePostTask(Context context){
            this.mContext = context;
        }

        @Override
        protected String doInBackground(String... data) {

            DBTuitionFee db = new DBTuitionFee(mContext);
            List<StoredTuitionFee> listItems = db.getAllDeleteableTuitionFee();
            // Create a new HttpClient and Post Header

            for(int i=0; i< listItems.size(); i++) {

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(LINK_TYPE.FEE_DELETE);
                String ss = "";
                try {
                    //add data
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

                    nameValuePairs.add(new BasicNameValuePair("api_key", MSG_TYPE.API_KEY));
                    nameValuePairs.add(new BasicNameValuePair("tuition_fee_student_id", listItems.get(i).get_stdId() + ""));
                    nameValuePairs.add(new BasicNameValuePair("tuition_fee_date", listItems.get(i).get_date()));

                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    //execute http post
                    HttpResponse response = httpclient.execute(httppost);
                    ss = EntityUtils.toString(response.getEntity());
                    if (ss.equals(RESPONSE_TYPE.SUCCESS) || ss.equals(RESPONSE_TYPE.UPDATE_SUCCESS)) {
                        db.deleteTuitionFee(listItems.get(i));
                    }
                    else if(ss.equals(RESPONSE_TYPE.API_NOT_MATCH)){
                        break;
                    }
                } catch (ClientProtocolException e) {

                } catch (IOException e) {

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new TuitionFeePostTask(mContext).execute();
        }
    }

}
