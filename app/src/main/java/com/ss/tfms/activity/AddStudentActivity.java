package com.ss.tfms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ss.tfms.R;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.DBStudent;
import com.ss.tfms.database.DBTuitionFee;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.database.StoredBatch;
import com.ss.tfms.database.StoredStudent;
import com.ss.tfms.database.StoredTuitionFee;
import com.ss.tfms.fragment.StudentFragment;
import com.ss.tfms.fragment.StudentPaymentFragment;
import com.ss.tfms.utils.ZemSettings;

import java.util.Calendar;
import java.util.List;

/**
 * Created by sijansd on 2/17/2017.
 */

public class AddStudentActivity extends AppCompatActivity {

    EditText std_name;
    EditText std_num;
    Spinner blood_grp;
    EditText school;
    EditText className;
    EditText group;
    EditText parents_name;
    EditText parents_num;
    EditText address;
    EditText fee;
    Spinner batch_id;
    CheckBox cbPaidStatus;
    int paidStatus;
    StoredStudent strStudent;

    Intent intent;
    MenuItem itemSave,itemUpdate,itemEdit;
    static int selectedBatchId = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();

        std_name = (EditText)findViewById(R.id.et_std_name);
        std_num = (EditText)findViewById(R.id.et_std_num);
        blood_grp = (Spinner)findViewById(R.id.et_std_blood_grp);
        batch_id = (Spinner) findViewById(R.id.spnr_batch_id);
        fee = (EditText) findViewById(R.id.et_std_fee);
        school = (EditText)findViewById(R.id.et_school_name);
        className = (EditText)findViewById(R.id.et_class);
        group = (EditText)findViewById(R.id.et_group);
        parents_name = (EditText)findViewById(R.id.et_parents_name);
        parents_num = (EditText)findViewById(R.id.et_parents_num);
        address = (EditText)findViewById(R.id.et_arrd);
        cbPaidStatus = (CheckBox) findViewById(R.id.cBoxPaid);

        cbPaidStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    paidStatus = 1;
                }
                else{
                    paidStatus = 0;
                }
            }
        });

        DBBatch batchDB = new DBBatch(this);

        int bId = getIntent().getIntExtra(StudentPaymentFragment.INTENT_BATCH_ID_KEY, -1);
        int batchID =-1;
        StoredBatch sb = null;
        if(bId != -1){
            sb = batchDB.getSingleBatch(bId);
        }


        List<StoredBatch> itemList = batchDB.getAllBatchNames();
        String[] ss = new String[itemList.size()];
        for (int i = 0; i < itemList.size(); i++) {
            ss[i] = itemList.get(i).get_batchName();
            if(sb!=null && sb.get_batchName().equals(itemList.get(i).get_batchName())){
                batchID = i;
                break;
            }
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ss);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        batch_id.setAdapter(spinnerArrayAdapter);

        if(batchID != -1){
            batch_id.setSelection(batchID);
        }

        strStudent = (StoredStudent) intent.getSerializableExtra(StudentFragment.STUDENT_EDIT_KEY);
        if(strStudent != null){
            setupEditUi(false);
            getSupportActionBar().setTitle("");
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_student_menu,menu);
         itemSave = menu.findItem(R.id.action_save);
         itemUpdate = menu.findItem(R.id.action_update);
         itemEdit = menu.findItem(R.id.action_edit);

        if(strStudent != null){
            itemSave.setVisible(false);
            itemEdit.setVisible(true);

        }
        else{
            itemSave.setVisible(true);
            itemEdit.setVisible(false);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                finish();
                break;

            case R.id.action_save:
                if(checkFieldState().equals("")){
                    addStudent();
                }
                else{
                    Toast.makeText(getApplicationContext(),checkFieldState(),Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.action_edit:
                itemEdit.setVisible(false);
                itemUpdate.setVisible(true);
                setupEditUi(true);
                break;

            case R.id.action_update:

                if(checkFieldState().equals("")){
                    updateStudent();
                    itemEdit.setVisible(true);
                    itemUpdate.setVisible(false);
                }
                else{
                    Toast.makeText(getApplicationContext(),checkFieldState(),Toast.LENGTH_SHORT).show();
                }

                break;

            default:
                    break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void addStudent(){

        DBBatch batchDB = new DBBatch(this);
        List<StoredBatch> itemList = batchDB.getAllBatchNames();
        int batchId = 0;

        for(int i=0; i<itemList.size(); i++){
            if(batch_id.getSelectedItem().toString().equals(itemList.get(i).get_batchName())){
                batchId = itemList.get(i).get_batchId();
                break;
            }
        }

        StoredStudent student = new StoredStudent();

        student.set_std_batch_id(batchId);
        student.set_std_name(std_name.getText().toString());
        student.set_std_num(std_num.getText().toString());
        student.set_std_blood_grp(blood_grp.getSelectedItem().toString());
        student.set_std_school(school.getText().toString());
        student.set_std_class(className.getText().toString());
        student.set_std_group(group.getText().toString());
        student.set_std_parents_name(parents_name.getText().toString());
        student.set_std_parents_num(parents_num.getText().toString());
        student.set_std_address(address.getText().toString());
        student.set_std_fee(fee.getText().toString());
        student.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);

        DBStudent db = new DBStudent(this);
        db.addStudent(student);
        int lastId = db.getLastRowId();

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        String date = "";

        mMonth++;
        String[] monthAry = getResources().getStringArray(R.array.month_names);
        for(int i=0; i< monthAry.length; i++){
            if(mMonth == i) {
                date = monthAry[i] + "-" + mYear;
                break;
            }
        }

        DBTuitionFee dbTuitionFee = new DBTuitionFee(this);
        dbTuitionFee.addTuitionFee(new StoredTuitionFee(0,lastId, paidStatus, date, ServerUpLoad.MSG_TYPE.ADD_STATUS));

        updateBatchInfo(batchId);
    }



    private void updateStudent(){

        DBBatch batchDB = new DBBatch(this);
        List<StoredBatch> itemList = batchDB.getAllBatchNames();
        int batchId = 0;

        for(int i=0; i<itemList.size(); i++){
            if(batch_id.getSelectedItem().toString().equals(itemList.get(i).get_batchName())){
                batchId = itemList.get(i).get_batchId();
                break;
            }
        }


        StoredStudent student = (StoredStudent) intent.getSerializableExtra(StudentFragment.STUDENT_EDIT_KEY);

        student.set_std_batch_id(batchId);
        student.set_std_name(std_name.getText().toString());
        student.set_std_num(std_num.getText().toString());
        student.set_std_blood_grp(blood_grp.getSelectedItem().toString());
        student.set_std_school(school.getText().toString());
        student.set_std_class(className.getText().toString());
        student.set_std_group(group.getText().toString());
        student.set_std_parents_name(parents_name.getText().toString());
        student.set_std_parents_num(parents_num.getText().toString());
        student.set_std_address(address.getText().toString());
        student.set_std_fee(fee.getText().toString());
        student.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);

        DBStudent db = new DBStudent(this);
        db.updateStudent(student);

        updateBatchInfo(batchId);

    }

    private void updateBatchInfo(int batchID){

        DBStudent sdb = new DBStudent(this);
        int std_count = sdb.getStudentCount(batchID);

        DBBatch bdb = new DBBatch(this);
        StoredBatch sb = bdb.getSingleBatch(batchID);
        sb.set_studentCount(std_count+"");
        sb.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);
        bdb.updateBatch(sb);

        ZemSettings zem = new ZemSettings(this);
        selectedBatchId = zem.getCurrentBatch();
        if(selectedBatchId > 0){
            int batchid = selectedBatchId;
            zem.setCurrentBatch(0);
            zem.save();

            updateBatchInfo(batchid);

        }

        finish();
    }


    private void setupEditUi(boolean bol){

        StoredStudent item = (StoredStudent) intent.getSerializableExtra(StudentFragment.STUDENT_EDIT_KEY);

        DBBatch batchDB = new DBBatch(this);
        List<StoredBatch> itemList = batchDB.getAllBatchNames();
        int batchID = 0;
        for(int i=0; i< itemList.size(); i++){
            if(item.get_std_batch_id() == itemList.get(i).get_batchId()){
                batchID = i;

                if(bol){
                    ZemSettings zem = new ZemSettings(this);
                    zem.setCurrentBatch(itemList.get(i).get_batchId());
                    zem.save();
                }

                break;
            }
        }


        String[] bldNames = getResources().getStringArray(R.array.blood_names);
        int bldId = 0;

            for (int i = 1; i < bldNames.length; i++) {
                if (item.get_std_blood_grp().equals(bldNames[i])){
                    bldId = i;
                    break;
                }
        }



        std_name.setFocusableInTouchMode(bol);
        std_num.setFocusableInTouchMode(bol);
        blood_grp.setEnabled(bol);
        batch_id.setEnabled(bol);
        school.setFocusableInTouchMode(bol);
        className.setFocusableInTouchMode(bol);
        group.setFocusableInTouchMode(bol);
        parents_name.setFocusableInTouchMode(bol);
        parents_num.setFocusableInTouchMode(bol);
        address.setFocusableInTouchMode(bol);
        fee.setFocusableInTouchMode(false);
        cbPaidStatus.setVisibility(View.GONE);

        std_name.setText(item.get_std_name());
        std_num.setText(item.get_std_num());
        blood_grp.setSelection(bldId);
        batch_id.setSelection(batchID);
        school.setText(item.get_std_school());
        className.setText(item.get_std_class());
        group.setText(item.get_std_group());
        parents_name.setText(item.get_std_parents_name());
        parents_num.setText(item.get_std_parents_num());
        address.setText(item.get_std_address());
        fee.setText(item.get_std_fee());


    }


    private String checkFieldState(){

        if(std_name.getText().toString().equals("")){
            return getResources().getString(R.string.name_field_state);
        }

        if(std_num.getText().toString().equals("")){
            return getResources().getString(R.string.std_num_state);
        }

        if(className.getText().toString().equals("")){
            return getResources().getString(R.string.std_class_state);
        }

        if(fee.getText().toString().equals("")){
            return getResources().getString(R.string.std_fee_state);
        }

        if(parents_name.getText().toString().equals("")){
            return getResources().getString(R.string.parents_name_state);
        }

        if(parents_num.getText().toString().equals("")){
            return getResources().getString(R.string.parents_num_state);
        }


        return "";
    }

}
