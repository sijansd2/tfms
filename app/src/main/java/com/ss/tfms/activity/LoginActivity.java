package com.ss.tfms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ss.tfms.R;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.utils.ZemSettings;

/**
 * Created by sijansd on 4/29/2017.
 */

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        final ZemSettings zem = new ZemSettings(this);

        final EditText et_pass = (EditText) findViewById(R.id.pass);
        Button login = (Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(et_pass.getText().toString().trim().equals(ServerUpLoad.MSG_TYPE.API_KEY)){
                    zem.setPass(et_pass.getText().toString());
                    zem.save();

                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Wrong Password!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
