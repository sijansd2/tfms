package com.ss.tfms.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ss.tfms.R;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.DBStudent;
import com.ss.tfms.database.DBTuitionFee;
import com.ss.tfms.database.ServerDownload;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.utils.Connectivity;
import com.ss.tfms.utils.ZemSettings;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Handler.Callback {

    public static TypedArray itemIcons;
    public static String[] home_page_item_names;
    public static String[] home_page_item_hints;
    TextView stdCount,batchCount, updateText;
    public static Handler handler;


    public static class MSG_TYPE{
        public static int UPLOAD_SUCCESS = 1121;
        public static int DOWNLOAD_SUCCESS = 1122;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ZemSettings zem = new ZemSettings(this);
        if(!zem.getPass().equals(ServerUpLoad.MSG_TYPE.API_KEY)){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        handler = new Handler(this);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        stdCount = (TextView)findViewById(R.id.stdCount);
        batchCount = (TextView)findViewById(R.id.batchCount);
        updateText = (TextView)findViewById(R.id.updateText);
        final Activity act = this;

        updateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ServerUpLoad.BatchDeletePostTask(act).execute();
            }
        });


        RelativeLayout backDrop = (RelativeLayout)findViewById(R.id.backdrop);
        backDrop.getLayoutParams().height = height/2 - 100;

        //navigation drawer part

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        home_page_item_names = this.getResources().getStringArray(R.array.home_page_item_names);
        home_page_item_hints = this.getResources().getStringArray(R.array.home_page_item_hints);
        itemIcons = getResources().obtainTypedArray(R.array.home_page_item_images);

        RecyclerView rv = (RecyclerView)findViewById(R.id.recyclerview);
        rv.setLayoutManager(new GridLayoutManager(rv.getContext(), 1));
        rv.setAdapter(new SimpleStringRecyclerViewAdapter(this));

    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        DBBatch dbBatch = new DBBatch(this);
        DBTuitionFee dbTuitionFee = new DBTuitionFee(this);
        if(dbBatch.getBatchCount() == 0){
            if(Connectivity.isConnected(getApplicationContext())){
                //updateText.setVisibility(View.GONE);
                new ServerDownload.LoadBatchInfo(this).execute();
            }else{
                setNetworkSetting();
            }

        }else if(dbBatch.getUpdateableBatchCount() > 0 || dbBatch.getDeleteableBatchCount()>0 || dbTuitionFee.getUpdateableFeeCount()>0){
            if(Connectivity.isConnected(getApplicationContext())){
                //uploadToServer();
                updateText.setVisibility(View.VISIBLE);
            }
        }
        setStdAndBatch();
    }

    public void setNetworkSetting(){
        new AlertDialog.Builder(this)
                .setTitle("Attention!")
                .setMessage("Please connect with network for internal configure.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert).setCancelable(false)
                .show();
    }

//    public void uploadToServer(){
//        new AlertDialog.Builder(this)
//                .setTitle("Notice!")
//                .setMessage("You need to upload your recent information")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        new ServerUpLoad.BatchDeletePostTask(getApplicationContext()).execute();
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert).setCancelable(false)
//                .show();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private List<String> getSublist(String[] array, int amount) {
        ArrayList<String> list = new ArrayList<>(amount);
        for(int i=0; i<amount; i++){
            list.add(array[i]);
        }
        return list;
    }


    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        //private int mBackground;
        //private List<String> mValues;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mBoundString;

            public final View mView;
            public final ImageView mImageView;
            public final TextView mTextView;
            public final TextView mTextHint;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mImageView = (ImageView) view.findViewById(R.id.avatar);
                mTextView = (TextView) view.findViewById(android.R.id.text1);
                mTextHint = (TextView) view.findViewById(R.id.textHint);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTextView.getText();
            }
        }

        public String getValueAt(int position) {
            return home_page_item_names[position];
        }

        public SimpleStringRecyclerViewAdapter(Context context) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            //mBackground = mTypedValue.resourceId;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_home, parent, false);
            // view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mBoundString = home_page_item_names[position];
            holder.mTextView.setText(home_page_item_names[position]);
            holder.mTextHint.setText(home_page_item_hints[position]);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    itemClick(context,  position);
                }
            });

//            Glide.with(holder.mImageView.getContext())
//                    .load(new Items().getCategoryDrawable(position))
//                    .fitCenter()
//                    .into(holder.mImageView);

            Picasso.with(holder.mImageView.getContext()).load(itemIcons.getResourceId(position, -1)).into(holder.mImageView);
        }

        @Override
        public int getItemCount() {
            return home_page_item_names.length;
        }


        private void itemClick(Context mContext, int pos){

            switch (pos){

                case 0:
                    Intent intent = new Intent(mContext, BatchActivity.class);
                    mContext.startActivity(intent);
                    break;

                case 1:
                    Intent intent1 = new Intent(mContext, StudentActivity.class);
                    mContext.startActivity(intent1);
                    break;

                case 2:
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://navedimperio.com/tmsphpfiles/aboutus.html"));
                    mContext.startActivity(browserIntent);
                    //new ServerUpLoad.FeeDeletePostTask(mContext).execute();
                    break;


                default:
                    break;
            }
        }
    }


    public void setStdAndBatch(){
        DBStudent dbStudent = new DBStudent(this);
        stdCount.setText(dbStudent.getAllStudentCount()+"");

        DBBatch dbBatch = new DBBatch(this);
        batchCount.setText(dbBatch.getBatchCount()+"");

    }

    @Override
    public boolean handleMessage(Message msg) {

        if(msg.what == MSG_TYPE.UPLOAD_SUCCESS){
            updateText.setVisibility(View.GONE);
        }
        if(msg.what == MSG_TYPE.DOWNLOAD_SUCCESS){
            setStdAndBatch();
        }
        return false;
    }

}
