package com.ss.tfms.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ss.tfms.R;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.fragment.StudentFragment;
import com.ss.tfms.fragment.TuitionFeeFragment;

/**
 * Created by sijansd on 2/17/2017.
 */

public class StudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag_holder);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager().beginTransaction().add(R.id.containerView, new StudentFragment(), "Tuitionfee").commit();

    }

}
