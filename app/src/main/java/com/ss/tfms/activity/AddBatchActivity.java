package com.ss.tfms.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ss.tfms.R;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.StoredBatch;
import com.ss.tfms.fragment.BatchFragment;

import java.util.Calendar;

/**
 * Created by sijansd on 2/17/2017.
 */

public class AddBatchActivity extends AppCompatActivity implements View.OnClickListener{


    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;

    EditText name;
    TextView startTime;
    TextView endTime;
    TextView etTemp = null;
    String format;

    Spinner day1, day2,day3;
    Spinner start_month, end_month;
    Intent intent;

    int hour;
    int min;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_batch);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = getIntent();

        name = (EditText)findViewById(R.id.et_name);
        startTime = (TextView)findViewById(R.id.tv_start_time);
        endTime = (TextView)findViewById(R.id.tv_end_time);

        startTime.setOnClickListener(this);
        endTime.setOnClickListener(this);

        day1 = (Spinner)findViewById(R.id.spn_day1);
        day2 = (Spinner)findViewById(R.id.spn_day2);
        day3 = (Spinner)findViewById(R.id.spn_day3);

        day1.setPrompt("Day 1");

        start_month = (Spinner)findViewById(R.id.spn_month_start);
        end_month = (Spinner)findViewById(R.id.spn_month_end);


        //dateView = (TextView) findViewById(R.id.textView3);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        min = calendar.get(Calendar.MINUTE);


        if(intent.getExtras()!=null){
            setupEditUi();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_student_menu,menu);
        MenuItem itemSave = menu.findItem(R.id.action_save);
        MenuItem itemUpdate = menu.findItem(R.id.action_update);

        if(intent.getExtras() != null){
            itemSave.setVisible(false);
            itemUpdate.setVisible(true);
        }
        else{
            itemSave.setVisible(true);
            itemUpdate.setVisible(false);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                finish();
                break;

            case R.id.action_save:
                if(checkFieldState().equals("")){
                    saveData();
                }
                else{
                    Toast.makeText(getApplicationContext(),checkFieldState(),Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.action_update:
                if(checkFieldState().equals("")){
                    updateData();
                }
                else{
                    Toast.makeText(getApplicationContext(),checkFieldState(),Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.action_search:
                break;

            default:
                    break;
        }

        return super.onOptionsItemSelected(item);
    }




    public void setTime(View view) {
        showDialog(888);
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub

        if(id == 888){
            final TimePickerDialog tpd = new TimePickerDialog(this, myTimeListener, hour, min, false);
            tpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        tpd.dismiss();
                        etTemp.setText("");
                    }
                }
            });
            return tpd;
        }

        return null;
    }

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            showTime(etTemp ,hourOfDay, minute);

        }
    };


    public void showTime(TextView v, int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        v.setText(new StringBuilder().append(hour).append(":").append(min)
                .append(" ").append(format));
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_start_time:
                etTemp = startTime;
                setTime(getCurrentFocus());
                break;

            case R.id.tv_end_time:
                etTemp = endTime;
                setTime(getCurrentFocus());
                break;

            default:
                break;
        }
    }


    public void setupEditUi(){

        StoredBatch item = (StoredBatch) intent.getSerializableExtra(BatchFragment.BATCH_EDIT_KEY);

        name.setText(item.get_batchName());

        String days = item.get_batchDay();
        String[] dayArray = days.split(" - ");
        String[] dayNames = getResources().getStringArray(R.array.day_names);
        Spinner[] spinners = {day1, day2, day3};

        for(int j=0; j<dayArray.length; j++) {
            for (int i = 1; i < dayNames.length; i++) {
                if (dayArray[j].equals(dayNames[i])){
                    spinners[j].setSelection(i);
                    break;
                }
            }
        }


        String months = item.get_batchMonth();
        String[] monthArray = months.split(" - ");
        String[] monthNames = getResources().getStringArray(R.array.month_names);
        Spinner[] spinnerMonths = {start_month, end_month};

        for(int j=0; j<monthArray.length; j++) {
            for (int i = 1; i < monthNames.length; i++) {
                if (monthArray[j].equals(monthNames[i])){
                    spinnerMonths[j].setSelection(i);
                    break;
                }
            }
        }


        String[] time = item.get_batchTime().split(" - ");
        if(item != null){
            startTime.setText(time[0]);
            endTime.setText(time[1]);
        }

    }

    public void saveData(){

        DBBatch db = new DBBatch(this);
        StoredBatch storedBatch = new StoredBatch();
        String days;

        if(day3.getSelectedItem().toString().equals("Day")){

             days = day1.getSelectedItem().toString()+" - "+day2.getSelectedItem().toString();
        }
        else {
             days = day1.getSelectedItem().toString()+" - "+day2.getSelectedItem().toString()+" - "+day3.getSelectedItem().toString();

        }

        String months = start_month.getSelectedItem().toString()+" - "+end_month.getSelectedItem().toString();
        String time = startTime.getText().toString()+" - "+endTime.getText().toString();

        storedBatch.set_batchName(name.getText().toString());
        storedBatch.set_batchDay(days);
        storedBatch.set_batchMonth(months);
        storedBatch.set_batchTime(time);
        storedBatch.set_studentCount("0");
        storedBatch.set_srv_status(1);

        db.addBatch(storedBatch);

        finish();
    }

    private void updateData() {

        DBBatch db = new DBBatch(this);
        StoredBatch storedBatch = (StoredBatch) intent.getSerializableExtra(BatchFragment.BATCH_EDIT_KEY);
        String days = day1.getSelectedItem().toString()+" - "+day2.getSelectedItem().toString()+" - "+day3.getSelectedItem().toString();
        String months = start_month.getSelectedItem().toString()+" - "+end_month.getSelectedItem().toString();
        String time = startTime.getText().toString()+" - "+endTime.getText().toString();

        storedBatch.set_batchName(name.getText().toString());
        storedBatch.set_batchDay(days);
        storedBatch.set_batchMonth(months);
        storedBatch.set_batchTime(time);
        //storedBatch.set_studentCount("0");
        storedBatch.set_srv_status(1);

        db.updateBatch(storedBatch);

        finish();
    }

    private String checkFieldState(){

        if(name.getText().toString().equals("")){
            return getResources().getString(R.string.name_field_state);
        }

        if(day1.getSelectedItem().toString().equals("Day")){
            return getResources().getString(R.string.day1_field_state);
        }

        if(day2.getSelectedItem().toString().equals("Day")){
            return getResources().getString(R.string.day2_field_state);
        }

        if(start_month.getSelectedItem().toString().equals("Month")){
            return getResources().getString(R.string.sMonth_field_state);
        }

        if(end_month.getSelectedItem().toString().equals("Month")){
            return getResources().getString(R.string.eMonth_field_state);
        }

        if(startTime.getText().toString().equals("")){
            return getResources().getString(R.string.sTime_field_state);
        }

        if(endTime.getText().toString().equals("")){
            return getResources().getString(R.string.eTime_field_state);
        }

            return "";
    }

}
