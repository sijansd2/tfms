package com.ss.tfms.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ZemSettings {

	private SharedPreferences _prefs = null;
	private Editor _editor = null;

	private int _currentBatchId = 0;
	private String _stdSms = "";
	private String _pass = "";
	private int _lastBatchCount = 0;
	private int _lastStdCount = 0;
	private int _lastFeeCount = 0;

	public ZemSettings(Context context) {

		this._prefs = context.getSharedPreferences("PREFS_PRIVATE",
				Context.MODE_PRIVATE);
		this._editor = this._prefs.edit();
	}



	public void setCurrentBatch(int o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putInt("_currentBatchId", o);
	}

	public int getCurrentBatch() {
		if (this._prefs == null) {
			return -1;
		}
		this._currentBatchId = this._prefs.getInt("_currentBatchId", -1);
		return this._currentBatchId;
	}


	public void setStdSms(String o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putString("_stdSms", o);
	}

	public String getStdSms() {
		if (this._prefs == null) {
			return "";
		}
		this._stdSms = this._prefs.getString("_stdSms", "");
		return this._stdSms;
	}



	public void setLastBatchCount(int o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putInt("_lastBatchCount", o);
	}

	public int getLastBatchCount() {
		if (this._prefs == null) {
			return 0;
		}
		this._lastBatchCount = this._prefs.getInt("_lastBatchCount", _lastBatchCount);
		return this._lastBatchCount;
	}


	public void setLastStdCount(int o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putInt("_lastStdCount", o);
	}

	public int getLastStdCount() {
		if (this._prefs == null) {
			return 0;
		}
		this._lastStdCount = this._prefs.getInt("_lastStdCount", _lastStdCount);
		return this._lastStdCount;
	}


	public void setLastFeeCount(int o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putInt("_lastFeeCount", o);
	}

	public int getLastFeeCount() {
		if (this._prefs == null) {
			return 0;
		}
		this._lastFeeCount = this._prefs.getInt("_lastFeeCount", _lastFeeCount);
		return this._lastFeeCount;
	}


	public void setPass(String o) {
		if (this._editor == null) {
			return;
		}
		this._editor.putString("_pass", o);
	}

	public String getPass() {
		if (this._prefs == null) {
			return "";
		}
		this._pass = this._prefs.getString("_pass", "");
		return this._pass;
	}





	public void save() {
		if (this._editor == null) {
			return;
		}
		this._editor.commit();
	}
}
