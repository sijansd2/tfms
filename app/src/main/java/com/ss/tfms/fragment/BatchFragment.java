package com.ss.tfms.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ss.tfms.R;
import com.ss.tfms.activity.AddBatchActivity;
import com.ss.tfms.activity.StudentPaymentActivity;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.DBStudent;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.database.StoredBatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sijansd on 2/19/2017.
 */

public class BatchFragment extends Fragment {

    RecyclerView rv;
    EditText search;
    DBBatch db;
    public final static String BATCH_EDIT_KEY = "batch_edit";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_recycle_holder, container, false);
        rv = (RecyclerView)rootView.findViewById(R.id.batch_recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        search = (EditText)rootView.findViewById(R.id.ed_search);
        setSearch();

        db = new DBBatch(getActivity());

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
        new LoadBatches().execute();
    }


    //*********************adapter part **********************

    public class BatchAdapter extends RecyclerView.Adapter<BatchAdapter.ViewHolder> implements Filterable {
        private List<StoredBatch> listItems;
        Context mContext;
        private ValueFilter valueFilter;
        List<StoredBatch> filterItems;
        public  ArrayList<HashMap<String,String>> list;



        public BatchAdapter(Context context, List<StoredBatch> data) {
            listItems = data;
            list = new ArrayList<HashMap<String, String>>();
            filterItems = listItems;

            for (int i=0; i<listItems.size(); i++){
                HashMap<String,String> row =  new HashMap<String, String>();
                row.put("tick","");
                list.add(row);
            }

        }


        @Override
        public BatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_batch, parent, false);

            BatchAdapter.ViewHolder vh = new BatchAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final BatchAdapter.ViewHolder holder, final int position) {

            holder.lay_counter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showStd(listItems.get(position));
                }

            });

            holder.parentLay.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    showModifyDlg(listItems.get(position));
                    return false;
                }
            });


            holder.tvBatchName.setText(listItems.get(position).get_batchName());
            holder.tvBatchDay.setText(listItems.get(position).get_batchDay());
            holder.tvBatchMonth.setText(listItems.get(position).get_batchMonth());
            holder.tvBatchTime.setText(listItems.get(position).get_batchTime());
            holder.tvStdnCount.setText(listItems.get(position).get_studentCount());
        }



        @Override
        public int getItemCount() {
            return listItems.size();
        }


        public  class ViewHolder extends RecyclerView.ViewHolder {

            public TextView tvBatchName;
            public TextView tvBatchDay;
            public TextView tvBatchTime;
            public TextView tvBatchMonth;
            public TextView tvStdnCount;
            public ImageView imvSms;
            CardView cv;
            LinearLayout lay_counter;
            RelativeLayout parentLay;


            public ViewHolder(View v) {
                super(v);

                cv = (CardView)v.findViewById(R.id.card_view);
                tvBatchName =(TextView)v.findViewById(R.id.tv_batch_name);
                tvBatchDay =(TextView)v.findViewById(R.id.tv_day);
                tvBatchTime =(TextView)v.findViewById(R.id.tv_time);
                tvBatchMonth =(TextView)v.findViewById(R.id.tv_month);
                tvStdnCount = (TextView)v.findViewById(R.id.tv_students_counts);
                imvSms = (ImageView)v.findViewById(R.id.imv_sms);
                lay_counter = (LinearLayout)v.findViewById(R.id.student_counter);
                parentLay = (RelativeLayout)v.findViewById(R.id.parent_lay);

            }

        }

        @Override
        public Filter getFilter() {

            if(valueFilter==null) {
                valueFilter=new ValueFilter();
            }
            return valueFilter;
        }

        private class ValueFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results=new FilterResults();
                if(constraint!=null && constraint.length()>0){

                    List<StoredBatch> filterList = new ArrayList<StoredBatch>();

                    for(int i=0;i<filterItems.size();i++){

                        if((filterItems.get(i).get_batchName().toLowerCase()).contains(constraint.toString().toLowerCase()) ||
                                (filterItems.get(i).get_batchName().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (filterItems.get(i).get_batchDay().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                    (filterItems.get(i).get_batchDay().toLowerCase()).contains(constraint.toString().toLowerCase())) {

                            StoredBatch std = new StoredBatch();
                            std.set_batchId(filterItems.get(i).get_batchId());
                            std.set_batchName(filterItems.get(i).get_batchName());
                            std.set_batchMonth(filterItems.get(i).get_batchMonth());
                            std.set_batchTime(filterItems.get(i).get_batchTime());
                            std.set_batchDay(filterItems.get(i).get_batchDay());
                            std.set_studentCount(filterItems.get(i).get_studentCount());

                            filterList.add(std);
                        }
                    }
                    results.count=filterList.size();
                    results.values=filterList;
                }else{
                    results.count=filterItems.size();
                    results.values=filterItems;
                }
                return results;
            }


            //Invoked in the UI thread to publish the filtering results in the user interface.
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listItems=(ArrayList<StoredBatch>) results.values;
                mAdapter.notifyDataSetChanged();
            }
        }

    }


    //********************* bg loading part **********************

    private List<StoredBatch> listItems;
    private BatchAdapter mAdapter;
    //Get all roster from database
    public class LoadBatches extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            listItems = db.getAllBatches();
            if(listItems != null) {
                mAdapter = new BatchAdapter(getActivity(), listItems);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            rv.setAdapter(mAdapter);
        }
    }


    /**** modify dialog *****/
    public void showModifyDlg(final StoredBatch item){

        final Activity act = getActivity();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(act);
        // builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle("Select One Option");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_item);
        arrayAdapter.add("Edit");
        arrayAdapter.add("Delete");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                if(strName.equals("Delete")){
                    deleteItem(item);
                }
                else if(strName.equals("Edit")){
                    editItem(item);
                }

                //Toast.makeText(act,"okok",Toast.LENGTH_SHORT).show();
            }
        });
        builderSingle.show();
    }

    public void deleteItem(final StoredBatch item){
        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.batch_delete_title))
                .setMessage(getResources().getString(R.string.batch_delete_details))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        item.set_srv_status(ServerUpLoad.MSG_TYPE.DELETE_STATUS);
                        db.updateBatch(item);
                        DBStudent sdb = new DBStudent(getActivity());
                        sdb.deleteAllStudentOfBatch(item.get_batchId());
                        new LoadBatches().execute();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void showStd(final StoredBatch item){

        Intent intent1 = new Intent(getActivity(), StudentPaymentActivity.class);
        intent1.putExtra(StudentPaymentFragment.INTENT_BATCH_ID_KEY, item.get_batchId());
        getActivity().startActivity(intent1);

    }

    public void editItem(final StoredBatch item){

        Intent intent  = new Intent(getActivity(), AddBatchActivity.class);
        intent.putExtra(BATCH_EDIT_KEY,item);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.batch_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){

            case android.R.id.home:
                if(search.getVisibility() == View.VISIBLE){
                    search.setVisibility(View.GONE);
                    hideKeyBoard();
                }else{
                    getActivity().finish();
                }

                break;

            case R.id.action_search:
                search.setVisibility(View.VISIBLE);
                break;

            case R.id.action_add:
                Intent intent3 = new Intent(getActivity(), AddBatchActivity.class);
                startActivity(intent3);
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void setSearch(){

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public  void hideKeyBoard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
