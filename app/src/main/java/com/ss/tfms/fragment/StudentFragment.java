package com.ss.tfms.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ss.tfms.R;
import com.ss.tfms.activity.AddBatchActivity;
import com.ss.tfms.activity.AddStudentActivity;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.DBStudent;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.database.StoredBatch;
import com.ss.tfms.database.StoredStudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sijansd on 2/19/2017.
 */

public class StudentFragment extends Fragment {

    DBStudent db;
    RecyclerView rv;
    EditText search;
    public final static String STUDENT_EDIT_KEY = "student_edit";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBStudent(getActivity());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_recycle_holder, container, false);

        rv = (RecyclerView)rootView.findViewById(R.id.batch_recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        search = (EditText)rootView.findViewById(R.id.ed_search);
        setSearch();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadStudents().execute();
    }

    //********************* bg loading part **********************

    private List<StoredStudent> listItems;
    private StudentAdapter mAdapter;
    //Get all roster from database
    public class LoadStudents extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            listItems = db.getAllStudents();
            if(listItems != null) {
                mAdapter = new StudentAdapter(getActivity(), listItems);

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            rv.setAdapter(mAdapter);
        }
    }



    //*********************adapter part **********************

    public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> implements Filterable {
        private List<StoredStudent> listItems;
        Context mContext;
        private ValueFilter valueFilter;
        List<StoredStudent> filterItems;
        public  ArrayList<HashMap<String,String>> list;



        public StudentAdapter(Context context, List<StoredStudent> data) {
            listItems = data;
            list = new ArrayList<HashMap<String, String>>();
            filterItems = listItems;

            for (int i=0; i<listItems.size(); i++){
                HashMap<String,String> row =  new HashMap<String, String>();
                row.put("tick","");
                list.add(row);
            }

        }


        @Override
        public StudentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_student, parent, false);

            StudentAdapter.ViewHolder vh = new StudentAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final StudentAdapter.ViewHolder holder, final int position) {

            holder.parentLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editItem(listItems.get(position));
                }

            });

            holder.parentLay.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    showModifyDlg(listItems.get(position));
                    return false;
                }
            });

            holder.name.setText(listItems.get(position).get_std_name());
            holder.batch_name.setText(listItems.get(position).get_std_num());
        }



        @Override
        public int getItemCount() {
            return listItems.size();
        }


        public  class ViewHolder extends RecyclerView.ViewHolder {

            public TextView name;
            public TextView batch_name;
            LinearLayout parentLay;


            public ViewHolder(View v) {
                super(v);

                name =(TextView)v.findViewById(R.id.name);
                batch_name =(TextView)v.findViewById(R.id.batch_name);
                parentLay = (LinearLayout)v.findViewById(R.id.parent_lay);

            }

        }


        @Override
        public Filter getFilter() {

            if(valueFilter==null) {
                valueFilter=new ValueFilter();
            }
            return valueFilter;
        }

        private class ValueFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results=new FilterResults();
                if(constraint!=null && constraint.length()>0){

                    List<StoredStudent> filterList = new ArrayList<StoredStudent>();

                    for(int i=0;i<filterItems.size();i++){

                        if((filterItems.get(i).get_std_name().toLowerCase()).contains(constraint.toString().toLowerCase()) ||
                                (filterItems.get(i).get_std_name().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (filterItems.get(i).get_std_num().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (filterItems.get(i).get_std_num().toLowerCase()).contains(constraint.toString().toLowerCase())) {

                            StoredStudent student = new StoredStudent();
                            student.set_std_id(filterItems.get(i).get_std_id() );
                            student.set_std_batch_id(filterItems.get(i).get_std_batch_id());
                            student.set_std_name(filterItems.get(i).get_std_name());
                            student.set_std_num(filterItems.get(i).get_std_num());
                            student.set_std_blood_grp(filterItems.get(i).get_std_blood_grp());
                            student.set_std_school(filterItems.get(i).get_std_school());
                            student.set_std_class(filterItems.get(i).get_std_class());
                            student.set_std_group(filterItems.get(i).get_std_group());
                            student.set_std_parents_name(filterItems.get(i).get_std_parents_name());
                            student.set_std_parents_num(filterItems.get(i).get_std_parents_num());
                            student.set_std_address(filterItems.get(i).get_std_address());
                            student.set_std_fee(filterItems.get(i).get_std_fee());

                            filterList.add(student);
                        }
                    }
                    results.count=filterList.size();
                    results.values=filterList;
                }else{
                    results.count=filterItems.size();
                    results.values=filterItems;
                }
                return results;
            }


            //Invoked in the UI thread to publish the filtering results in the user interface.
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listItems=(ArrayList<StoredStudent>) results.values;
                mAdapter.notifyDataSetChanged();
            }
        }

    }

    /**** modify dialog *****/
    public void showModifyDlg(final StoredStudent item){

        final Activity act = getActivity();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(act);
        // builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle("Select One Option");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_item);
        //arrayAdapter.add("Edit");
        arrayAdapter.add("Delete");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                if(strName.equals("Delete")){
                    deleteItem(item);
                }
                else if(strName.equals("Edit")){
                    editItem(item);
                }

                //Toast.makeText(act,"okok",Toast.LENGTH_SHORT).show();
            }
        });
        builderSingle.show();
    }

    public void deleteItem(final StoredStudent item){
        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.student_delete_title))
                .setMessage(getResources().getString(R.string.student_delete_details))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        item.set_srv_status(ServerUpLoad.MSG_TYPE.DELETE_STATUS);
                        db.updateStudent(item);
                        updateBatchInfo(item);
                        new LoadStudents().execute();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void editItem(final StoredStudent item){

        Intent intent  = new Intent(getActivity(), AddStudentActivity.class);
        intent.putExtra(STUDENT_EDIT_KEY,item);
        startActivity(intent);
    }


    private void updateBatchInfo(StoredStudent item){

        DBStudent sdb = new DBStudent(getActivity());
        int std_count = sdb.getStudentCount(item.get_std_batch_id());

        DBBatch bdb = new DBBatch(getActivity());
        StoredBatch sb = bdb.getSingleBatch(item.get_std_batch_id());
        sb.set_studentCount(std_count+"");
        sb.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);
        bdb.updateBatch(sb);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.batch_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){

            case android.R.id.home:
                if(search.getVisibility() == View.VISIBLE){
                    search.setVisibility(View.GONE);
                    hideKeyBoard();
                }else{
                    getActivity().finish();
                }
                break;

            case R.id.action_add:
                //Toast.makeText(getApplicationContext(),"New student add window coming soon",Toast.LENGTH_SHORT).show();
                DBBatch dbBatch = new DBBatch(getActivity());
                int size = dbBatch.getAllBatches().size();
                if(size>0){
                    Intent intent3 = new Intent(getActivity().getApplicationContext(), AddStudentActivity.class);
                    startActivity(intent3);
                }
                else{
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Attention! Batch not found.")
                            .setMessage("To add student first you need to add a batch. Please goto 'Batches' option and add new batch using (+) sign.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

                break;

            case R.id.action_search:
                search.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setSearch(){

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public  void hideKeyBoard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
