package com.ss.tfms.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ss.tfms.R;
import com.ss.tfms.activity.StudentPaymentActivity;
import com.ss.tfms.database.StoredBatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sijansd on 2/19/2017.
 */

public class  TuitionFeeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_recycle_holder, container, false);

        List<StoredBatch> items = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            StoredBatch sb = new StoredBatch();

            items.add(sb);
        }

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.batch_recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setAdapter(new TuitionFeeAdapter(getContext(), items));


        return rootView;
    }





    //*********************adapter part **********************
    public class TuitionFeeAdapter extends RecyclerView.Adapter<TuitionFeeAdapter.ViewHolder> {
        private List<StoredBatch> listItems;
        Context mContext;
        public ArrayList<HashMap<String, String>> list;


        public TuitionFeeAdapter(Context context, List<StoredBatch> data) {
            listItems = data;
            list = new ArrayList<HashMap<String, String>>();

            for (int i = 0; i < listItems.size(); i++) {
                HashMap<String, String> row = new HashMap<String, String>();
                row.put("tick", "");
                list.add(row);
            }
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_tuition_fee, parent, false);

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), StudentPaymentActivity.class);
                    v.getContext().startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {


            public TextView tvBatchName;
            public TextView tvBatchDay;
            public TextView tvBatchTime;
            public TextView tvBatchMonth;
            public ImageView imvSms;
            CardView cv;


            public ViewHolder(View v) {
                super(v);
                cv = (CardView) v.findViewById(R.id.card_view);
                tvBatchName = (TextView) v.findViewById(R.id.tv_batch_name);
                tvBatchDay = (TextView) v.findViewById(R.id.tv_day);
                tvBatchTime = (TextView) v.findViewById(R.id.tv_time);
                tvBatchMonth = (TextView) v.findViewById(R.id.tv_month);
                imvSms = (ImageView) v.findViewById(R.id.imv_sms);

            }

        }

    }
}
