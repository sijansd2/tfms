package com.ss.tfms.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ss.tfms.R;
import com.ss.tfms.activity.AddStudentActivity;
import com.ss.tfms.database.DBBatch;
import com.ss.tfms.database.DBStudent;
import com.ss.tfms.database.DBTuitionFee;
import com.ss.tfms.database.ServerUpLoad;
import com.ss.tfms.database.StoredBatch;
import com.ss.tfms.database.StoredStudent;
import com.ss.tfms.database.StoredTuitionFee;
import com.ss.tfms.utils.ZemSettings;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sijansd on 2/19/2017.
 */

public class StudentPaymentFragment extends Fragment {

    DBTuitionFee db;
    RecyclerView rv;
    Spinner spnMonth, spnYear;
    int batchId;
    EditText search;
    TextView tvTotal;
    boolean isSendSmsToAll = false;
    public static String INTENT_BATCH_ID_KEY = "batchidkey";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        db = new DBTuitionFee(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        getDate();
        //new StudentPaymentFragment.LoadStudents().execute(getDate());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_std_payment, container, false);

        rv = (RecyclerView)rootView.findViewById(R.id.batch_recyclerview);
        tvTotal = (TextView) rootView.findViewById(R.id.tv_total);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        search = (EditText)rootView.findViewById(R.id.ed_search);
        setSearch();

        spnMonth = (Spinner)rootView.findViewById(R.id.spn_month);
        spnYear = (Spinner)rootView.findViewById(R.id.spn_year);

        spnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String date = spnMonth.getSelectedItem().toString()+"-"+spnYear.getSelectedItem().toString();
                new StudentPaymentFragment.LoadStudents().execute(date);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String date = spnMonth.getSelectedItem().toString()+"-"+spnYear.getSelectedItem().toString();
                new StudentPaymentFragment.LoadStudents().execute(date);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        batchId = getActivity().getIntent().getIntExtra(StudentPaymentFragment.INTENT_BATCH_ID_KEY, -1);

        return rootView;
    }



    //********************* bg loading part **********************

    private List<StoredStudent> listItems;
    public StudentPaymentAdapter mAdapter;
    //Get all roster from database
    public class LoadStudents extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            listItems = db.getStudentsOfBatchId(batchId, params[0]);
            if(listItems != null) {
                mAdapter = new StudentPaymentAdapter(getActivity(), listItems);

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            rv.setAdapter(mAdapter);
        }
    }



    //*********************adapter part **********************

    public class StudentPaymentAdapter extends RecyclerView.Adapter<StudentPaymentAdapter.ViewHolder> implements Filterable {
        private List<StoredStudent> listItems;
        Context mContext;
        private ValueFilter valueFilter;
        List<StoredStudent> filterItems;
        public ArrayList<HashMap<String, String>> list;


        public StudentPaymentAdapter(Context context, List<StoredStudent> data) {
            listItems = data;
            list = new ArrayList<HashMap<String, String>>();
            filterItems = listItems;

            for (int i = 0; i < listItems.size(); i++) {
                HashMap<String, String> row = new HashMap<String, String>();
                row.put("tick", "");
                list.add(row);
            }

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_student_payment, parent, false);

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            holder.tvStdName.setText(listItems.get(position).get_std_name());
            holder.tvStdPhn.setText(listItems.get(position).get_std_num());
            holder.tvTaka.setText(listItems.get(position).get_std_fee()+" ৳");

             if(listItems.get(position).get_std_fee_status()==1){
                holder.tvTaka.setTextColor(getResources().getColor(android.R.color.holo_green_dark
                ));
            }else{
                holder.tvTaka.setTextColor(getResources().getColor(android.R.color.holo_red_light));
            }

            holder.layTaka.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                       dlgShowPayStatus(listItems.get(position), position);
                    return false;
                }
            });

            holder.imvSms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlgSmsOption(listItems.get(position), false);
                }
            });

            holder.cv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    deleteItem(listItems.get(position));
                    return false;
                }
            });
        }

        @Override
        public int getItemCount() {
            return listItems.size();
        }




        public class ViewHolder extends RecyclerView.ViewHolder {


            public TextView tvStdName;
            public TextView tvStdPhn;
            public TextView tvTaka;
            public LinearLayout layTaka;
            public ImageView imvSms;
            public CardView cv;


            public ViewHolder(View v) {
                super(v);

                tvStdName = (TextView) v.findViewById(R.id.tv_std_name);
                tvStdPhn = (TextView) v.findViewById(R.id.tv_std_phn);
                layTaka = (LinearLayout) v.findViewById(R.id.tv_taka_lay);
                tvTaka = (TextView) v.findViewById(R.id.tv_taka);
                imvSms = (ImageView) v.findViewById(R.id.imv_sms);
                cv = (CardView) v.findViewById(R.id.card_view);

            }

        }


        @Override
        public Filter getFilter() {

            if(valueFilter==null) {
                valueFilter=new ValueFilter();
            }
            return valueFilter;
        }

        private class ValueFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results=new FilterResults();
                if(constraint!=null && constraint.length()>0){

                    List<StoredStudent> filterList = new ArrayList<StoredStudent>();

                    for(int i=0;i<filterItems.size();i++){

                        if((filterItems.get(i).get_std_name().toLowerCase()).contains(constraint.toString().toLowerCase()) ||
                                (filterItems.get(i).get_std_name().toUpperCase()).contains(constraint.toString().toUpperCase()) ||
                                (filterItems.get(i).get_std_num().toUpperCase()).contains(constraint.toString().toUpperCase())) {

                            StoredStudent std = new StoredStudent();
                            std.set_std_id(filterItems.get(i).get_std_id());
                            std.set_std_batch_id(filterItems.get(i).get_std_batch_id());
                            std.set_std_name(filterItems.get(i).get_std_name());
                            std.set_std_num(filterItems.get(i).get_std_num());
                            std.set_std_fee(filterItems.get(i).get_std_fee());
                            std.set_std_fee_status(filterItems.get(i).get_std_fee_status());

                            filterList.add(std);
                        }
                    }
                    results.count=filterList.size();
                    results.values=filterList;
                }else{
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
                            new LoadStudents().execute(getDate());
                        }
                    });

                    results.count=filterItems.size();
                    results.values=filterItems;
                }
                return results;
            }


            //Invoked in the UI thread to publish the filtering results in the user interface.
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listItems=(ArrayList<StoredStudent>) results.values;
                mAdapter.notifyDataSetChanged();
            }
        }

    }


    /**** payment update dialog *****/
    public void dlgShowPayStatus(final StoredStudent item, final int position){

        final Activity act = getActivity();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(act);
        // builderSingle.setIcon(R.mipmap.ic_launcher);
        builderSingle.setTitle("Here you can change payment status of "+item.get_std_name());

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_item);

        if(item.get_std_fee_status()==1)
            arrayAdapter.add("Unpaid");
        else
            arrayAdapter.add("Paid");


        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                DBTuitionFee db = new DBTuitionFee(getActivity());
                StoredTuitionFee stf = new StoredTuitionFee();


                if(strName.equals("Paid")){
                    stf.set_stdId(item.get_std_id());
                    stf.set_status(1);
                    stf.set_date(spnMonth.getSelectedItem().toString()+"-"+spnYear.getSelectedItem().toString());
                    stf.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);
                    db.addTuitionFee(stf);
                    item.set_std_fee_status(1);
                    mAdapter.notifyDataSetChanged();
                    setTotal();
                    dialog.dismiss();
                }
                else if(strName.equals("Unpaid")){
                    stf.set_status(0);
                    stf.set_stdId(item.get_std_id());
                    stf.set_date(spnMonth.getSelectedItem().toString()+"-"+spnYear.getSelectedItem().toString());
                    stf.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);
                    db.updateTuitionFee(stf);
                    //db.deleteTuitionFee(stf);
                    item.set_std_fee_status(0);
                    mAdapter.notifyDataSetChanged();
                    setTotal();
                    dialog.dismiss();
                }

                //Toast.makeText(act,"okok",Toast.LENGTH_SHORT).show();
            }
        });
        builderSingle.show();
    }



    private String getDate(){

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        String year = mYear+"";
        String date = "";

        String[] monthAry = getResources().getStringArray(R.array.month_names);
        String[] yearAry = getResources().getStringArray(R.array.year_names);

        for(int i=0; i< monthAry.length; i++){
            if((mMonth+1) == i){
                spnMonth.setSelection(i);
                date = monthAry[i];
                break;
            }
        }

        for(int i=1; i< yearAry.length; i++){
            if(year.equals(yearAry[i])){
                spnYear.setSelection(i);
                date = date+"-"+year;
                break;
            }
        }

        return date;

    }


    /**** modify dialog *****/
    public void dlgSmsOption(final StoredStudent item, final boolean isSendSmsToAll){

        final Activity act = getActivity();
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(act);
        // builderSingle.setIcon(R.mipmap.ic_launcher);

        final String listItem1, listItem2;

        if(isSendSmsToAll) {
            builderSingle.setTitle("Send sms to all");
            listItem1 = "Students";
            listItem2 = "Parents";
        }
        else {
            listItem1 = "Student";
            listItem2 = "Parent";
            builderSingle.setTitle("Send sms to");
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(act, android.R.layout.select_dialog_item);
        arrayAdapter.add(listItem1);
        arrayAdapter.add(listItem2);

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                if(strName.equals(listItem1)){
                    if(isSendSmsToAll){
                        sendSmsToAllStd();
                    }else{
                        setSmsText(item.get_std_num());
                    }

                }
                else if(strName.equals(listItem2)){
                    if(isSendSmsToAll){
                        sendSmsToAllPrnts();
                    }else{
                        setSmsText(item.get_std_parents_num());
                    }

                }

                //Toast.makeText(act,"okok",Toast.LENGTH_SHORT).show();
            }
        });
        builderSingle.show();
    }


    public void setSmsText(final String num){

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dlg_sms_body);
        dialog.setTitle("Edit your SMS body.");

        final ZemSettings zem = new ZemSettings(getActivity());
        final EditText body = (EditText) dialog.findViewById(R.id.ed_body);
        body.setText(zem.getStdSms());

        Button dialogButton = (Button) dialog.findViewById(R.id.btnSave);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zem.setStdSms(body.getText().toString());
                zem.save();

                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", num);
                smsIntent.putExtra("sms_body",body.getText().toString());
                getActivity().startActivity(smsIntent);

                dialog.dismiss();
            }
        });

        Button cencel = (Button) dialog.findViewById(R.id.btnCancel);
        cencel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void sendSmsToAllStd(){
        String numbers = "";
        for(int i=0; i< listItems.size(); i++){
            numbers += listItems.get(i).get_std_num()+";";
        }

        setSmsText(numbers);

    }

    private void sendSmsToAllPrnts(){
        String numbers = "";
        for(int i=0; i< listItems.size(); i++){
            numbers += listItems.get(i).get_std_parents_num()+";";
        }

        setSmsText(numbers);

    }


    MenuItem showTotal;
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.student_pay_menu,menu);
        showTotal = menu.findItem(R.id.action_show_total);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()){

            case android.R.id.home:
                if(search.getVisibility() == View.VISIBLE){
                    search.setVisibility(View.GONE);
                    hideKeyBoard();
                }else{
                    getActivity().finish();
                }

                break;

            case R.id.action_send_to_all:
                dlgSmsOption(null, true);
                break;

//            case R.id.addStd:
//                Intent intent3 = new Intent(getActivity(), AddStudentActivity.class);
//                intent3.putExtra(INTENT_BATCH_ID_KEY,batchId);
//                startActivity(intent3);
//                break;

            case R.id.action_search:
                search.setVisibility(View.VISIBLE);
                tvTotal.setVisibility(View.GONE);
                showTotal.setTitle("Show Total");
                break;

            case R.id.action_show_total:
                if(tvTotal.getVisibility() == View.GONE){
                    tvTotal.setVisibility(View.VISIBLE);
                    showTotal.setTitle("Hide Total");
                    setTotal();
                    search.setText("");
                    search.setVisibility(View.GONE);
                }
                else{
                    tvTotal.setVisibility(View.GONE);
                    showTotal.setTitle("Show Total");
                }
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }






    public void setSearch(){

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public  void hideKeyBoard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void deleteItem(final StoredStudent item){
        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.student_delete_title))
                .setMessage(getResources().getString(R.string.student_delete_details))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        DBStudent db = new DBStudent(getActivity());
                        item.set_srv_status(ServerUpLoad.MSG_TYPE.DELETE_STATUS);
                        db.updateStudent(item);
                        updateBatchInfo(item);
                        new StudentPaymentFragment.LoadStudents().execute(getDate());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void updateBatchInfo(StoredStudent item){

        DBStudent sdb = new DBStudent(getActivity());
        int std_count = sdb.getStudentCount(item.get_std_batch_id());

        DBBatch bdb = new DBBatch(getActivity());
        StoredBatch sb = bdb.getSingleBatch(item.get_std_batch_id());
        sb.set_studentCount(std_count+"");
        sb.set_srv_status(ServerUpLoad.MSG_TYPE.ADD_STATUS);
        bdb.updateBatch(sb);

    }


    private void setTotal(){
        int taka =0;
        for(int i=0; i < listItems.size(); i++){
            if(listItems.get(i).get_std_fee_status() == 1)
             taka += Integer.parseInt(listItems.get(i).get_std_fee());
        }

        tvTotal.setText("Total: "+taka);
    }

}