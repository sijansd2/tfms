<?php
 
/*
 * Following code will list all the products
 */
 
$srv_key = "navedimperio";

$api_key = isset($_REQUEST["api_key"]) ? urldecode($_REQUEST["api_key"]) : '';


if($api_key == $srv_key){
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();
 
// get all products from products table
$result = mysql_query("SELECT *FROM table_tuition_fee") or die(mysql_error());
 
// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["table_tuition_fee"] = array();
 
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $product["tuition_fee_id"] = $row["tuition_fee_id"];
        $product["tuition_fee_student_id"] = $row["tuition_fee_student_id"];
		$product["tuition_fee_status"] = $row["tuition_fee_status"];
		$product["tuition_fee_date"] = $row["tuition_fee_date"];
 
        // push single product into final response array
        array_push($response["table_tuition_fee"], $product);
    }
    // success
    $response["success"] = 1;
 
    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No products found";
 
    // echo no users JSON
    echo json_encode($response);
}
}

else{
	echo "api_not_match";
}
?>
