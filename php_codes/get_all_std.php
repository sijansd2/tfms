<?php
 
/*
 * Following code will list all the products
 */
 
$srv_key = "navedimperio";

$api_key = isset($_REQUEST["api_key"]) ? urldecode($_REQUEST["api_key"]) : '';


if($api_key == $srv_key){
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db_connect.php';
 
// connecting to db
$db = new DB_CONNECT();
 
// get all products from products table
$result = mysql_query("SELECT *FROM table_student") or die(mysql_error());
 
// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    // products node
    $response["table_student"] = array();
 
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $product = array();
        $product["std_id"] = $row["std_id"];
        $product["std_batch_id"] = $row["std_batch_id"];
		$product["std_name"] = $row["std_name"];
		$product["std_num"] = $row["std_num"];
		$product["std_fee"] = $row["std_fee"];
		$product["std_parents_name"] = $row["std_parents_name"];
		$product["std_parents_num"] = $row["std_parents_num"];
		$product["std_address"] = $row["std_address"];
		$product["std_school"] = $row["std_school"];
		$product["std_class"] = $row["std_class"];
		$product["std_group"] = $row["std_group"];
		$product["std_blood_grp"] = $row["std_blood_grp"];
		$product["std_add_time"] = $row["std_add_time"];
 
        // push single product into final response array
        array_push($response["table_student"], $product);
    }
    // success
    $response["success"] = 1;
 
    // echoing JSON response
    echo json_encode($response);
} else {
    // no products found
    $response["success"] = 0;
    $response["message"] = "No products found";
 
    // echo no users JSON
    echo json_encode($response);
}
}

else{
	echo "api_not_match";
}
?>
